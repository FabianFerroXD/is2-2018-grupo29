<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Travel extends Model
{
    /**
     * The attributes that are mass assignable.
     * 
     * @var array
     */
    protected $fillable = [
        'car_id','user_id','departure_date','departure_time','cost',
        'departure_address','arrival_address','place_empty','arrival_date',
        'arrival_time','source_province','source_locality','destiny_province',
        'destiny_locality','multiple_id','multiple'
    ];

    public function users()
    {
        return $this->belongsToMany(User::class)->withTimestamps();
    }
    
    public function car()
    {
        return $this->belongsTo(Car::class);
    }

    public function questions()
    {
      return $this->hasMany(Question::class);
    }
    public function ratings()
    {
        return $this->hasMany(Rating::class);
    }

    public function selections()
    {
        return $this->hasMany(Selection::class);
    }
}
