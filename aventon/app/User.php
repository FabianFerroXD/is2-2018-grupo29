<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use App\Notifications\ResetPasswordNotification;
use Illuminate\Foundation\Auth\User as Authenticatable;



class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name','lastname','email','password','birthdate',
        'driver_qualification','passenger_qualification',
        'account','owedpenalized','owedqualification','low'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'
    ];

    public function cars()
    {
        return $this->belongsToMany(Car::class)->withTimestamps();
    }

    public function travels()
    {
        return $this->belongsToMany(Travel::class)->withTimestamps();
    }

    public function ratings()
    {
        return $this->hasMany(Rating::class);
    }

    public function selection()
    {
        return $this->hasOne(Rating::class);
    }

    public function questions()
    {
      return $this->hasMany(Question::class);
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        //$this->notify(new ResetPasswordNotification($token));
        return back()
            ->with('info', 'La respuesta fue enviada.');
    }
}
