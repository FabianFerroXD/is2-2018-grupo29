<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'patent','brand','model','year','kind','places'
    ];

    public function users()
    {
       return $this->belongsToMany(User::class)->withTimestamps();
    }

    public function travels()
    {
       return $this->hasMany(Travel::class);
    }
}