<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','travel_id','date_for_rating','user_to_rating','content','rate','type'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function travel()
    {
        return $this->belongsTo(Travel::class);
    }
    public function user_to_rating($rating)
    {
        return User::find($rating->user_to_rating);
    }
}
