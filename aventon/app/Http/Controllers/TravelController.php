<?php

namespace App\Http\Controllers;


use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Http\Requests\TravelRequest;
use Illuminate\Support\Facades\Auth;
use DateTime;
use Carbon\Carbon;

// APPS AVENTON
use App\Travel;
use App\Car;
use App\User;
use App\Selection;
use App\Rating;


class TravelController extends Controller
{
  /*
   * Funciones por defecto generadas por Route::resource en routes/web.php:
   * (index, store, create, destroy, update, show, edit).
   */

  public function index()
  {
    //
  }

  public function store(TravelRequest $request)
  {
    $user = Auth::user();
    $cost_travel = ($request->cost * 5) / 100;

    if( $user->account >= $cost_travel )
    {
      $departure_merge = new DateTime($request->departure_date .' ' .$request->departure_time);
      $arrival_merge = new DateTime($request->arrival_date .' ' .$request->arrival_time);

      // Valida que la partida sea menor que la de llegada.
      if( $arrival_merge < $departure_merge )
      {
        return back()
                ->with('info', 'Rango de fecha u hora incorrecto.')
                ->withInput();
      }
      else
      {
        // Valida que se encuentre en un rango de fecha disponible.
        $isInRange = $this->validateDate($departure_merge, $arrival_merge, $request->id);
        
        if( $isInRange )
        {
          $travel = new Travel;
          $car = Car::find($request->car_id); 

          $travel->car_id             = $car->id;
          $travel->user_id            = $user->id;
          $travel->departure_date     = $request->departure_date;
          $travel->arrival_date       = $request->arrival_date;
          $travel->departure_time     = $request->departure_time;
          $travel->arrival_time       = $request->arrival_time;
          $travel->cost               = $request->cost;
          $travel->departure_address  = $request->departure_address;
          $travel->arrival_address    = $request->arrival_address;
          $travel->place_empty        = $car->places;
          $travel->source_province    = $request->source_province;
          $travel->source_locality    = $request->source_locality;
          $travel->destiny_province   = $request->destiny_province;
          $travel->destiny_locality   = $request->destiny_locality;
          $travel->multiple           = $request->multiple;
          $travel->multiple_id        = $request->multiple_id;

          $travel->save();
          $user->travels()->save($travel);

          if($travel->multiple)
          {
              return back();
          }
          else
          {
              $user->decrement('account', $cost_travel);

              return redirect()
                  ->route('users.show', Auth::user())
                  ->with('info', 'El viaje ha sido creado');
          }
        }
        else
        {
          return back()
                  ->with('info', 'Hay un viaje en ese rango de fecha.')
                  ->withInput();
        }
      }
    }
    else
    {
      return back()
              ->with('info', 'No puedes dar de alta un viaje, porque tienes fondos insuficientes.')
              ->withInput();
    }
  }


  public function create()
  {
      $user   = Auth::user();
      $account_rating =0;
      foreach ($user->ratings as $rating)
      {
          if($rating->rate == 0 and $rating->date_for_rating < date("Y-m-d")) $account_rating++;
      }
      switch ($user) {
          case (count(Auth::user()->cars) == 0):
              return back()
                  ->with('info', 'No puedes dar de alta un viaje, no tenes autos');
              break;
          case ($user->account < 5):
              return back()
                  ->with('info', 'No puedes dar de alta un viaje, tienes fondos insuficientes.');
              break;
          case ($account_rating > 0 ):
              return back()
                  ->with('info', 'No puedes dar de alta un viaje, debes calificaciones ');
              break;
      }
      return view('travels.create');
  }


  // Primero se ejecuta validateDestroy.
  public function destroy($id)
  {
      $travel = Travel::find($id);
      $user   = Auth::user();



      // Elimina pasajeros y penaliza +1 por cada uno.
      if ( $travel->selections->count() != 0 )
      {
          foreach ($travel->selections as $selection )
          {
              $selection->delete();
              $user->decrement('driver_qualification');
          }
      }

      // Quita relación viaje-postulantes y penaliza +1.
      if ( $travel->users->count() > 1 )
      {
          foreach ($travel->users as $postulant )
          {
              $postulant->travels()->detach($travel->id);
          }

          $user->decrement('driver_qualification');
      }

      // Quita relación usuario-viaje.
      $user->travels()->detach($travel->id);

      // Elimina preguntas y respuestas.
      if ( $travel->questions->count() != 0 )
      {
          foreach ($travel->questions as $question )
          {
              $question->delete();
          }
      }

      // Elimina calificaciones teniendo encuenta ambos campos de user.
      if ( $travel->ratings->count() != 0 )
      {
          foreach ( Rating::all() as $rating )
          {
              if ( $rating->user_to_rating = $user->id or $rating->user_id = $user->id )
              {
                  $rating->delete();
              }
          }
      }

      $travel->delete();

      return redirect()
          ->route('users.show', Auth::user())
          ->with('info', 'El viaje a de la ciudad '.$travel->source_locality.' fue eliminado.');
  }


  public function update(TravelRequest $request, $id)
  {
    $travel = Travel::find($id);
    $hasPassangers = Selection::where('travel_id', $travel->id)->exists();
    $cantPostulants = $travel->users()->count();

    if ( $hasPassangers or $cantPostulants > 1)
    {
         return back()
              ->with('info', 'No puedes modificar el viaje, porque tienes pasajeros o postulantes en este viaje.');
    }
    else
    {
      $departure_merge = new DateTime($request->departure_date .' ' .$request->departure_time);
      $arrival_merge = new DateTime($request->arrival_date .' ' .$request->arrival_time);

      // Valida que la partida sea menor que la de llegada.
      if( $arrival_merge < $departure_merge )
      {
        return back()
                ->with('info', 'Rango de fecha u hora incorrecto.');
      }
      else
      {
        // Valida que se encuentre en un rango de fecha disponible.
        $isInRange = $this->validateDate($departure_merge, $arrival_merge, $id);
        
        if( $isInRange )
        {
          $car = Car::find($travel->car_id);

          $travel->departure_date     = $request->departure_date;
          $travel->arrival_date       = $request->arrival_date;
          $travel->departure_time     = $request->departure_time;
          $travel->arrival_time       = $request->arrival_time;
          $travel->cost               = $request->cost;
          $travel->departure_address  = $request->departure_address;
          $travel->arrival_address    = $request->arrival_address;
          $travel->place_empty        = $car->places;
          $travel->source_province    = $request->source_province;
          $travel->source_locality    = $request->source_locality;
          $travel->destiny_province   = $request->destiny_province;
          $travel->destiny_locality   = $request->destiny_locality;

          $travel->save();

          return redirect()
                  ->route('users.show', Auth::user())
                  ->with('info', 'El viaje con el auto de patente: ' . $travel->car->patent . ' ha sido actualizado');
        }
        else
        {
          return back()
                  ->with('info', 'Hay un viaje en ese rango de fecha.')
                  ->withInput();
        }
      }
    }
  }


  public function show($id)
  {
    $travel = Travel::find($id);

    if( $travel->multiple_id == NULL )
    {
      $is_driver = ( Auth::User()->id == $travel->user_id );

      if( !$is_driver )
      {
        $driver = User::find($travel->user_id);
      }

      $multiple_travel = Travel::where('multiple_id', '=', $id)->get();

      return view('travels.show', compact('travel', 'is_driver', 'driver', 'multiple_travel'));
    }
    else
    {
      return redirect()
              ->route('users.show', Auth::user())
              ->with('info', 'No tienes permitido acceder a ese viaje.');
    }
  }


  public function edit($id)
  {
    if( Auth::User()->travels()->where('travel_id', '=', $id)->exists() )
    {
      $travel = Travel::find($id);
      $is_editable = true;

      $hasPassangers = Selection::where('travel_id', $travel->id)->exists();
      $cantPostulants = $travel->users()->count();

      if ( $hasPassangers or $cantPostulants > 1)
      {
        return redirect()
                ->route('users.show', Auth::user())
                ->with('info', 'No puedes modificar el viaje, porque tienes pasajeros o postulantes en este viaje.');
      }

      return view('travels.edit', compact('travel', 'is_editable'));
    }
    else
    {
      return redirect()
              ->route('users.show', Auth::user())
              ->with('info', 'No tienes permitido acceder a ese viaje.');
    }
  }


  public function validateDestroy($id)
  {
    $travel = Travel::find($id);

    return view('travels.validate_destroy', compact('travel'));
  }


  public function showPostulants($id)
  {
    $travel = Travel::find($id);

    if( (Auth::User()->travels()->where('travel_id', '=', $id)->exists()) and ($travel->multiple_id == NULL) )
    {
      $multiple_travel = Travel::where('multiple_id', '=', $id)->get();

      return view('travels.show_postulants', compact('travel', 'multiple_travel'));
    }
    else
    {
      return redirect()
              ->route('users.show', Auth::user())
              ->with('info', 'No tienes permitido acceder a ese viaje.');
    }
  }


  public function showSelections($id)
  {
    $travel = Travel::find($id);

    if( (Auth::User()->travels()->where('travel_id', '=', $id)->exists()) and ($travel->multiple_id == NULL) )
    {

      $users = array();

      foreach( $travel->selections as $selection )
      {
        $user = User::find($selection->user_id);

        $users = array_add($users, $user->id, $user);
      }

      $multiple_travel = Travel::where('multiple_id', '=', $id)->get();

      return view('travels.show_selections', compact('travel', 'users', 'multiple_travel'));
    }
    else
    {
      return redirect()
              ->route('users.show', Auth::user())
              ->with('info', 'No tienes permitido acceder a ese viaje.');
    }
  }


  public function postulate($id)
  {
    $user = Auth::user();
    $travel = Travel::find($id);
    $car = Car::find($travel->car_id);
    $cont_ratings = $user->ratings()->where('user_id', '=', $user->id)->where('date_for_rating', '<', date('Y-m-d H:i:s'))->count();
    $pleace_cost = $travel->cost / $car->places;

    if( ($cont_ratings == 0) and ($pleace_cost <= $user->account) )
    {
      if( $user->travels()->where('travel_id', '=', $id)->exists()
          or $travel->selections()->where('user_id', '=', $user->id)->exists() )
      {
        return back()
            ->with('info', 'El usuario ya esta postulado a este viaje.');
      }
      else
      {
        $travel->users()->save($user);

        return back()
                ->with('info', 'Se te agrego a la lista de postulantes del viaje'.$id);
      }
    }
    else
    {
      return back()
              ->with('info', 'No puedes postularte al viaje '.$id.' porque debes calificaciones o tienes fondos insuficientes.');
    }
  }


  public function rejectPostulant($id, $user_id)
  {
    $user = User::find($user_id);

    $user->travels()->detach($id);

    return back()
        ->with('info', 'El postulante '. $user->id .' ha sido rechazado.');
  }


  public function acceptPostulant($id, $user_id)
  {
    $travel = Travel::find($id);
    $car = Car::find($travel->car_id);

    $cont_passanger = $travel->selections()->where('travel_id', '=', $id)->count();

    if( $cont_passanger < $car->places )
    {
      $passenger = User::find($user_id);

      $selection = factory(Selection::class)
            ->create(['user_id' => $passenger->id, 'travel_id' => $travel->id]);

      $travel->selections()->save($selection);
      $passenger->selection()->save($selection);

      $this->rejectPostulant($id, $user_id);

      $driver = Auth::user();

      factory(Rating::class)
           ->create([ 'user_id' => $driver->id, 'travel_id' => $travel->id, 'date_for_rating' => $travel->arrival_date, 'user_to_rating' => $passenger->id]);

      factory(Rating::class)
          ->create([ 'user_id' => $passenger->id, 'travel_id' => $travel->id, 'date_for_rating' => $travel->arrival_date, 'user_to_rating' => $driver->id]);

      $pleace_cost = $travel->cost / $car->places;

      $passenger->decrement('account', $pleace_cost);

      return back()
              ->with('info', 'El postulante '. $passenger->name .' ha sido aceptado.');
    }
    else
    {
      return back()
              ->with('info', 'El auto del viaje '.$id.' tiene las plazas llenas.');
    }
  }


  public function exitTravel($id, $user_id)
  {
    $user = User::find($user_id);

    $user->travels()->detach($id);

    return back()
        ->with('info', 'Has renunciado al viaje ' . $id);
  }

  public function exitTravelWithPenal($id, $user_id)
  {
    $user = User::find($user_id);
    $travel = Travel::find($id);

    $travel->selections()->where('user_id', '=', $user_id)->delete();

    $user->decrement('passenger_qualification');

    return back()
            ->with('info', 'Has renunciado al viaje ' . $id)
            ->with('info', 'Se te penalizo (-1).');
  }


  public function deletePassenger($id, $user_id)
  {
    $driver = Auth::user();
    $passenger = User::find($user_id);
    $travel = Travel::find($id);
    $car = Car::find($travel->car_id);

    $driver->ratings()->where('user_to_rating', '=', $passenger->id)->delete();
    $passenger->ratings()->where('user_to_rating', '=', $driver->id)->delete();

    $travel->selections()->where('user_id', '=', $passenger->id)->delete();

    $driver->decrement('driver_qualification');

    $pleace_cost = $travel->cost / $car->places;

    $passenger->increment('account', $pleace_cost);

    return back()
            ->with('info', 'Has sacado a un pasajero del viaje '.$id.'. Se te penalizo (-1).');
  }


  public function searchTravel(Request $request)
  {
    $travel = $request->search;

    $travels = Travel::where([
        ['destiny_locality', 'like', '%' . $travel . '%'],
        ['multiple_id', '=', NULL],
        ['arrival_date', '>', date('Y-m-d H:i:s')]
        ])->orwhere([
        ['source_locality', 'like', '%' . $travel . '%'],
        ['multiple_id', '=', NULL],
        ['arrival_date', '>', date('Y-m-d H:i:s')]
        ])
      ->orderBy('departure_date','ASC')->paginate();

    return view('travels.search_travel', compact('travels'));
  }


  public function showPassenger($id, $user_id)
  {
    $user = User::find($user_id);
    $travel = Travel::find($id);

    if( $user->travels()->where('travel_id', '=', $id)->exists() or ($travel->selections()->where('user_id', '=', $user->id)->exists()) )
    {
      return view('travels.show_passenger', compact('travel', 'user'));
    }
    else
    {
      return redirect()
              ->route('users.show', Auth::user())
              ->with('info', 'No tienes permitido acceder a esa información.');
    }
  }


  public function validateDate($departure_new, $arrival_new, $id)
  {
    $all_travels = Auth::user()->travels()->get();
    $travels = $all_travels->whereNotIn('id', $id);

    foreach ($travels as $travel)
    {
      $departure = new DateTime($travel->departure_date .' ' .$travel->departure_time);
      $arrival = new DateTime($travel->arrival_date .' ' .$travel->arrival_time);

      $isBefore = ( ($departure_new < $departure) and ($arrival_new < $departure) );
      $isAfter = ( $departure_new > $arrival );

      if ( !($isBefore or $isAfter) )
      {
        return false;
      }
    }

    return true;
  }

  public function storeMultiple(TravelRequest $request)
  {
    $departure = Carbon::parse($request->departure_date.''.$request->departure_time);

    $week_map = [
      0 => 'sunday',
      1 => 'monday',
      2 => 'tuesday',
      3 => 'wednesday',
      4 => 'thursday',
      5 => 'friday',
      6 => 'saturday',
    ];

    $week_day = $week_map[$departure->dayOfWeek];
    $work_day = collect($request->workday);

    if ($work_day->contains($week_day))
    {
      $user = Auth::user();
      $cost_travel = ($request->cost * 5) / 100;

      if( $user->account >= $cost_travel )
      {
        $old_request = $request;
        $duration = $request->duration ;
        $departure_new = Carbon::parse($request->departure_date.''.$request->departure_time);
        $arrival_new = Carbon::parse($request->arrival_date.''.$request->departure_time);

        //Validando
        for ($dia=$departure_new;$dia <= $arrival_new; $dia->addDay() )
        {
          $inicio = new DateTime($dia);
          $fin = new Carbon($dia);
          $fin = new DateTime($fin->addHour($duration));

          $is_in_range = $this->validateDate($inicio, $fin, $request->id);

          if ($dia->isMonday() and in_array('monday', $request->workday, true)){
              if (!$is_in_range) return back()->with('info', 'Hay un viaje en ese rango de fecha.')->withInput();
          } elseif ($dia->isTuesday() and in_array('tuesday', $request->workday, true)) {
              if (!$is_in_range) return back()->with('info', 'Hay un viaje en ese rango de fecha.')->withInput();
          } elseif ($dia->isWednesday() and in_array('wednesday', $request->workday, true)) {
              if (!$is_in_range) return back()->with('info', 'Hay un viaje en ese rango de fecha.')->withInput();
          } elseif ($dia->isThursday() and in_array('thursday', $request->workday, true)) {
              if (!$is_in_range) return back()->with('info', 'Hay un viaje en ese rango de fecha.')->withInput();
          } elseif ($dia->isFriday() and in_array('friday', $request->workday, true)) {
              if (!$is_in_range) return back()->with('info', 'Hay un viaje en ese rango de fecha.')->withInput();
          } elseif ($dia->isSaturday() and in_array('saturday', $request->workday, true)) {
              if (!$is_in_range) return back()->with('info', 'Hay un viaje en ese rango de fecha.')->withInput();
          } elseif ($dia->isSunday() and in_array('sunday', $request->workday, true)) {
              if (!$is_in_range) return back()->with('info', 'Hay un viaje en ese rango de fecha.')->withInput();
          }
        }

        //Creando
        $departure_new = Carbon::parse($old_request->departure_date.''.$old_request->departure_time);
        $arrival_new = Carbon::parse($old_request->arrival_date.''.$old_request->departure_time);

        $request->departure_date = $departure_new->format('Y-m-d');
        $request->departure_time = $departure_new->format('H:i:s');
        $old= new Carbon($departure_new);
        $fin = $old->addHour($duration);

        $request->arrival_date = $fin->format('Y-m-d');
        $request->arrival_time = $fin->format('H:i:s');

        $this->store($request);
        $main_id = Travel::where('departure_date', '=', $request->departure_date)->where('arrival_date', '=', $request->arrival_date)->get()->first()->id;

        $request->multiple_id = $main_id;

        for ($dia=$departure_new;$dia <= $arrival_new; $dia->addDay() )
        {
          $request->departure_date = $dia->format('Y-m-d');
          $request->departure_time = $dia->format('H:i:s');
          $old= new Carbon ($dia);
          $fin = $old->addHour($duration);
          $request->arrival_date = $fin->format('Y-m-d');
          $request->arrival_time = $fin->format('H:i:s');
          if ($dia->isMonday() and in_array('monday', $request->workday, true))$this->store($request);
          elseif ($dia->isTuesday() and in_array('tuesday', $request->workday, true))$this->store($request);
          elseif ($dia->isWednesday() and in_array('wednesday', $request->workday, true))$this->store($request);
          elseif ($dia->isThursday() and in_array('thursday', $request->workday, true))$this->store($request);
          elseif ($dia->isFriday() and in_array('friday', $request->workday, true))$this->store($request);
          elseif ($dia->isSaturday() and in_array('saturday', $request->workday, true)) $this->store($request);
          elseif ($dia->isSunday() and in_array('sunday', $request->workday, true))$this->store($request);
        }

        $user->account = $user->account - $cost_travel;
        $user->save();

        return redirect()
            ->route('users.show', Auth::user())
            ->with('info', 'El viaje multiple ha sido creado');
      }
      else
      {
        return back()
                ->with('info', 'No puedes dar de alta un viaje, porque tienes fondos insuficientes.')
                ->withInput();
      }
    }
    else
    {
      return back()
              ->with('info', 'El día de la fecha de partida no coincide con ninguno de los días seleccionados.')
              ->withInput();
    }
  }



  public function createMultiple()
  {
      $user   = Auth::user();
      $account_rating =0;
      foreach ($user->ratings as $rating)
      {
          if($rating->rate == 0 and $rating->date_for_rating < date("Y-m-d")) $account_rating++;
      }
      switch ($user) {
          case (count(Auth::user()->cars) == 0):
              return back()
                  ->with('info', 'No puedes dar de alta un viaje, no tenes autos');
              break;
          case ($user->account < 5):
              return back()
                  ->with('info', 'No puedes dar de alta un viaje, tienes fondos insuficientes.');
              break;
          case ($account_rating > 0 ):
              return back()
                  ->with('info', 'No puedes dar de alta un viaje, debes calificaciones ');
              break;
      }
      return view('travels.create_multiple');
  }


  public function editMultiple($id)
  {
    if( Auth::User()->travels()->where('travel_id', '=', $id)->exists() )
    {
      $travel = Travel::find($id);
      $ult_travel = Travel::where('multiple_id', '=', $travel->id)->get()->last();
      $travel->arrival_date = $ult_travel->departure_date;
      $is_editable = true;

      $hasPassangers = Selection::where('travel_id', $travel->id)->exists();
      $cantPostulants = $travel->users()->count();

      if ( $hasPassangers or $cantPostulants > 1)
      {
        return redirect()
                ->route('users.show', Auth::user())
                ->with('info', 'No puedes modificar el viaje, porque tienes pasajeros o postulantes en este viaje.');
      }

      return view('travels.edit_multiple', compact('travel', 'is_editable'));
    }
    else
    {
      return redirect()
              ->route('users.show', Auth::user())
              ->with('info', 'No tienes permitido acceder a ese viaje.');
    }
  }


  public function updateMultiple(TravelRequest $request, $id)
  {
    $travel = Travel::find($id);
    $hasPassangers = Selection::where('travel_id', $travel->id)->exists();
    $cantPostulants = $travel->users()->count();

    if ( $hasPassangers or $cantPostulants > 1)
    {
         return back()
              ->with('info', 'No puedes modificar el viaje, porque tienes pasajeros o postulantes en este viaje.');
    }
    else
    {
      $travel->departure_address  = $request->departure_address;
      $travel->arrival_address    = $request->arrival_address;
      $travel->source_province    = $request->source_province;
      $travel->source_locality    = $request->source_locality;
      $travel->destiny_province   = $request->destiny_province;
      $travel->destiny_locality   = $request->destiny_locality;

      $travel->save();


      $travels = Travel::where('multiple_id', '=', $travel->id)->get();

      foreach ($travels as $travel)
      {
        $travel->departure_address  = $request->departure_address;
        $travel->arrival_address    = $request->arrival_address;
        $travel->source_province    = $request->source_province;
        $travel->source_locality    = $request->source_locality;
        $travel->destiny_province   = $request->destiny_province;
        $travel->destiny_locality   = $request->destiny_locality;

        $travel->save();
      }

        return redirect()
                ->route('users.show', Auth::user())
                ->with('info', 'El viaje con el auto de patente: ' . $travel->car->patent . ' ha sido actualizado');

    }
  }

}