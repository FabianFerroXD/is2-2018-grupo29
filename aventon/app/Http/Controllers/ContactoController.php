<?php 

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class ContactoController {

    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function contact()
    {
        return view('contact.show_contact');
    }

}