<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CarRequest;
use Illuminate\Support\Facades\Auth;

// APPS AVENTON
use App\Car;
use App\User;
use App\Travel;


class CarController extends Controller
{
  /*
   * Funciones por defecto generadas por Route::resource en routes/web.php:
   * (index, store, create, destroy, update, show, edit).
   */

  public function index()
  {
    //
  }


  public function store(CarRequest $request)
  {
    $car =  new Car;

    $car->patent  = $request->patent;
    $car->brand   = $request->brand;
    $car->model   = $request->model;
    $car->year    = $request->year;
    $car->kind    = $request->kind;
    $car->places  = $request->places;
    $car->save();

    /*
     * $car->users()->attach(Auth::user()->id);
     * El sync es igual a attach pero el attach(si lo ejecutas 2 veces
     * los crea 2 veces).
     *
     * Agregar if si el auto existe utilizar attach, y si no existe se
     * utiliza sync.
     */

    $car->users()->sync(Auth::user()->id);
    return redirect()
            ->route('users.show', Auth::user())
            ->with('info', 'El auto '.$car->brand.' fue guardado.');
  }


  public function create()
  {
    return view('cars.create');
  }


  public function destroy($id)
  {
    if ( Travel::where('car_id', '=', $id)->exists() )
    {
      return back()
              ->with('info', 'Tiene viajes pendientes.');
    }
    else
    {
      $car = Car::find($id);
      $car->users()->detach(Auth::user()->id);
      $car->delete();

      return back()
              ->with('info', 'El auto '.$car->brand.' fue eliminado.');
    }
  }


  public function update(CarRequest $request, $id)
  {
    if( Travel::where('car_id', '=', $id)->exists() )
    {
      return back()
              ->with('info', 'No se puede modificar el auto '.$id.' por que tiene viajes pendientes.');
    }
    else
    {
      $car = Car::find($id);

      $car->patent  = $car->patent;
      $car->brand   = $request->brand;
      $car->model   = $request->model;
      $car->year    = $request->year;
      $car->kind    = $request->kind;
      $car->places  = $request->places;

      $car->save();

      return redirect()
              ->route('users.show', Auth::user())
              ->with('info', 'El auto '.$car->brand.' fue actualizado.');
    }
  }


  public function show($id)
  {
    if ( Auth::User()->cars()->where('car_id', '=', $id)->exists() )
    {
      $car = Car::find($id);
      return view('cars.show',compact('car'));
    }
    else
    {
      return redirect()
              ->route('users.show', Auth::user())
              ->with('info', 'No tienes permitido acceder a ese auto.');
    }
  }


  public function edit($id)
  {
    if ( Auth::User()->cars()->where('car_id', '=', $id)->exists() )
    {
      $car = Car::find($id);
      return view('cars.edit',compact('car'));
    }
    else
    {
      return redirect()
              ->route('users.show', Auth::user())
              ->with('info', 'No tienes permitido acceder a ese auto.');
    }
  }
}