<?php

namespace App\Http\Controllers;

use App\Providers\AuthServiceProvider;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Auth;

// APPS AVENTON
use App\User;
use App\Travel;


class HomeController extends Controller
{
    public function index()
    {
        $travels = Travel::where([
          ['multiple_id', '=', NULL],
          ['arrival_date', '>', date('Y-m-d H:i:s')],
          ['user_id', '!=', Auth::user()->id]
        ])->orderBy('departure_date','ASC')->paginate(7);

        return view('home', compact('travels'));
    }
}