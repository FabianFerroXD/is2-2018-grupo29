<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

// APPS AVENTON
use App\Rating;
use App\User;
use App\Travel;

class RatingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($user_id)
    {
      if( Auth::user()->id == $user_id )
      {
        $ratings = Rating::where('user_id', '=', $user_id)->orderBy('date_for_rating','ASC')->where('date_for_rating', '<', date('Y-m-d H:i:s'))->paginate(7);

        return view('ratings.show', compact('ratings'));
      }
      else
      {
        return redirect()
                ->route('users.show', Auth::user())
                ->with('info', 'No tienes permitido entrar al rating de ese usario.');
      }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $rating = Rating::find($id);
      $user_to_rating = User::find($rating->user_to_rating);
      $message = '';

      switch($request->qualification)
      {
          case 'Positive':
              return $this->ratePositive($request, $id);
              $message = 'Has calificado a positivamente a ';
              break;
          case 'Neutral':
              $this->rateNeutral($request, $id);
              $message = 'Has calificado a neutralmente a ';
              break;
          case 'Negative':
              $this->rateNegative($request, $id);
              $message = 'Has calificado a negativamente a ';
              break;
      }

      return redirect()
              ->route('ratings.show', Auth::user())
              ->with('info', $message.$user_to_rating->name);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $id->delete();
    }

    public function ratePositive(Request $request,$id)
    {
        $rating = Rating::find($id);
        $travel = Travel::find($rating->travel_id);
        $user_to_rating = User::find($rating->user_to_rating);


        if( $travel->user_id == $user_to_rating->id )
        {
            $user_to_rating->increment('driver_qualification');
        }
        else
        {
            $user_to_rating->increment('passenger_qualification');
        }

        $rating->content=$request->content;
        $rating->rate=true;
        $rating->type="Positiva";
        $rating->save();
        return back()
            ->with('info', 'Has calificado a positivamente a '.$user_to_rating->name);
    }


    public function rateNegative(Request $request,$id)
    {
        $rating = Rating::find($id);
        $travel = Travel::find($rating->travel_id);
        $user_to_rating = User::find($rating->user_to_rating);

        if( $travel->user_id == $user_to_rating->id )
        {
            $user_to_rating->decrement('driver_qualification');
        }
        else
        {
            $user_to_rating->decrement('passenger_qualification');
        }

        $rating->content=$request->content;
        $rating->rate=true;
        $rating->type="Negativa";
        $rating->save();
        return back()
            ->with('info', 'Has calificado negativamente a '.$user_to_rating->name);
    }


    public function rateNeutral(Request $request,$id)
    {
        $rating = Rating::find($id);
        $user_to_rating = User::find($rating->user_to_rating);

        $rating->content=$request->content;
        $rating->rate=true;
        $rating->type="Neutral";
        $rating->save();
        return back()
            ->with('info', 'Has dado una calificación neutra a ' . $user_to_rating->name);
    }


    public function showRatings($user_id)
    {
        if( Auth::user()->id == $user_id )
        {
            $ratings = Rating::where('user_to_rating', '=', $user_id)->orderBy('date_for_rating','ASC')
                                ->where('rate', '=', true)->paginate();

            return view('ratings.show_ratings', compact('ratings'));
        }
        else
        {
            return redirect()
                ->route('users.show', Auth::user())
                ->with('info', 'No tienes permitido entrar al rating de ese usario.');
        }
    }
}
