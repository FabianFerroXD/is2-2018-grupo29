<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

// APPS AVENTON
use App\Question;
use App\User;
use App\Travel;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {//La funcion fea esta recive un travel_id , question->travel_id , $question->id  y $question->user_id

      $travel = Travel::find($request->question_travel);
      $user = Auth::User();

      if( $travel->user_id == $user->id)
      {
          $answer = new Question;
          $question = Question::find($request->question_id);
          $answer->user_id      = $user->id;
          $answer->question_id  = $question->id;
          $answer->answer_id    = NULL;
          $answer->travel_id    = $travel->id;
          $answer->content      = $request->content;
          $answer->answer       = true;
          $answer->save();
          $travel->questions()->save($answer);
          $user->questions()->save($answer);
          $question->answer = false;
          $question->answer_id = $answer->id;
          $question->update();
          return back()
              ->with('info', 'La respuesta fue enviada.');
      }
      else{
          $question = new Question;
          $question->user_id      = $user->id;
          $question->question_id  = NULL;
          $question->answer_id    = NULL;
          $question->travel_id    = $travel->id;
          $question->content      = $request->content;
          $question->answer       = false;
          $question->save();
          $travel->questions()->save($question);
          $user->questions()->save($question);
          return back()
              ->with('info', 'La pregunta fue enviada.');
         }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
