<?php

namespace App\Http\Controllers;

use App\Providers\AuthServiceProvider;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Auth;

// APPS AVENTON
use App\User;
use App\Car;
use App\Travel;
use App\Selection;
use App\Rating;

class UserController extends Controller
{
  /*
   * Funciones por defecto generadas por Route::resource en routes/web.php:
   * (index, store, create, destroy, update, show, edit).
   */

  public function index()
  {
    //
  }

  /*
   * Ni store, ni create se utilizan.
   * RegisterController hace estas funciones.
   */

  public function store(UserRequest $request)
  {
    //
  }


  public function create()
  {
    //
  }


  // Primero se ejecuta validateDestroy.
  public function destroy($id)
  {
    $user = User::find($id);
    $travels = Travel::where('user_id', '=', $user->id)->get();
    $cars = $user->cars()->where('user_id', '=', $user->id)->get();

    foreach( $travels as $travel )
    {
      $this->destroyTravel($travel->id);
    }

    foreach( $cars as $car )
    {
      $this->destroyCar($car->id);
    }

    User::where('id',$user->id)->delete();

    return redirect('/')
            ->with('info', 'El usuario fue eliminado.');
  }


  public function update(UserRequest $request, $id)
  {
    $user = Auth::User();

    if( $user->id == $id )
    {
      $user->name       = $request->name;
      $user->lastname   = $request->lastname;
      $user->birthdate  = $request->birthdate;

      $user->save();

      return redirect()
              ->route('users.show', $user)
              ->with('info', 'El usuario '.$user->name.' fue actualizado.');
    }
    else
    {
      return redirect('/')
              ->with('info', 'Acceso denegado.');
    }
  }


  public function show($id)
  {
    $user = Auth::User();

    if( $user->id == $id )
    {
      $drive_travels = Travel::where('user_id', '=', $id)->where('multiple_id', '=', NULL)->where('arrival_date', '>=', date('Y-m-d H:i:s'))->get();

      $postulant_travels = $user->travels()->where('multiple_id', '=', NULL)->where('arrival_date', '>', date('Y-m-d H:i:s'))->get();

      $selection_travels = array();
      $selections = Selection::where('user_id', $user->id)->get();

      foreach( $selections as $selection )
      {
        $travel = Travel::find($selection->travel_id);

        if(!$travel->multiple)
        {
          $selection_travels = array_add($selection_travels, $travel->id, $travel);
        }

        if( ($travel->multiple_id == NULL) and ($travel->multiple) )
        {
          $sub_travels = Travel::where('multiple_id', '=', $travel->id)->get();
          $max_date_travel = $sub_travels->max('arrival_date');

          if( $max_date_travel > date('Y-m-d H:i:s') )
          {
            $selection_travels = array_add($selection_travels, $travel->id, $travel);
          }
        }
      }

      return view('users.show', compact('user', 'drive_travels', 'postulant_travels', 'selection_travels'));
    }
    else
    {
      return redirect('/')
              ->with('info', 'Acceso denegado.');
    }
  }


  public function edit($id)
  {
    $user = Auth::User();

    if( $user->id == $id )
    {
      $user = User::find($id);
      return view('users.edit', compact('user'));
    }
    else
    {
      return redirect('/')
              ->with('info', 'Acceso denegado.');
    }
  }

  /*
   * Demás funciones...
   */

  public function validateDestroy($id)
  {
      $user = Auth::User();
      if( $user->id == $id )
      {
          $hasRatings = $user->ratings()->where('date_for_rating', '<', date('Y-m-d H:i:s'))->exists();
          if( $hasRatings )
          {
              return back()
                  ->with('info', 'No puedes borrar tu cuenta, porque debes calificaciones.');
          }
          else
          {
              $travels = $user->travels()->get();
              foreach ($travels as $travel)
              {

                  if($travel->arrival_date > date('Y-m-d H:i:s'))
                  {
                    
                    $hasPassangers = Selection::where('travel_id', $travel->id)->exists();
                    if ( $hasPassangers )
                    {
                         return back()
                              ->with('info', 'No puedes borrar tu cuenta, porque tienes pasajeros en algún viaje.');
                    }

                  }
              }
          }
          return view('users.validate_destroy', compact('user'));
      }
      else
      {
          return redirect('/')
              ->with('info', 'Acceso denegado.');
      }
  }



  public function editPassword($id)
  {
    $user = Auth::User();

    if( $user->id == $id )
    {
      return view('users.edit_password', compact('user'));
    }
    else
    {
      return redirect('/')
              ->with('info', 'Acceso denegado.');
    }
  }


  public function updatePassword(UserRequest $request, $id)
  {
    $user = Auth::User();

    if( $user->id == $id )
    {
      $user->password = bcrypt($request->password);

      $user->save();

      return redirect()
              ->route('users.show', $user)
              ->with('info', 'La contraseña fue actualizada.');
    }
    else
    {
      return redirect('/')
              ->with('info', 'Acceso denegado.');
    }
  }


  public function destroyTravel($id)
    {
        $travel = Travel::find($id);
        $user   = Auth::user();

        // Elimina pasajeros y penaliza +1 por cada uno.
        if ( $travel->selections->count() != 0 )
        {
            foreach ($travel->selections as $selection )
            {
                $selection->delete();
                $user->decrement('driver_qualification');
            }
        }

        // Quita relación viaje-postulantes y penaliza +1.
        if ( $travel->users->count() > 1 )
        {
            foreach ($travel->users as $postulant )
            {
                $postulant->travels()->detach($travel->id);
            }

            $user->decrement('driver_qualification');
        }

        // Quita relación usuario-viaje.
        $user->travels()->detach($travel->id);

        // Elimina preguntas y respuestas.
        if ( $travel->questions->count() != 0 )
        {
            foreach ($travel->questions as $question )
            {
                $question->delete();
            }
        }

        // Elimina calificaciones teniendo encuenta ambos campos de user.
        if ( $travel->ratings->count() != 0 )
        {
            foreach ( Rating::all() as $rating )
            {
                if ( $rating->user_to_rating = $user->id or $rating->user_id = $user->id )
                {
                    $rating->delete();
                }
            }
        }

        $travel->delete();
    }

    public function destroyCar($id)
    {
        $car = Car::find($id);
        $car->users()->detach(Auth::user()->id);
        $car->delete();
    }

}