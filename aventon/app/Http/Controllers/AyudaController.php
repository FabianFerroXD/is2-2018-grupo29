<?php 

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

class AyudaController {

    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function ayuda()
    {
        return view('ayuda.show_ayuda');
    }

}