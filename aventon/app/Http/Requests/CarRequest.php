<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CarRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'patent'    => 'string|max:7|min:7|unique:cars',
            'brand'     => 'required|string|max:50',
            'model'     => 'required|string|max:50',
            'year'      => 'required|int|Min:2000|Max:2019',
            'kind'      => 'required|in:Auto,Camioneta,Colectivo,Trafic,Camion',
            'places'    => 'required|int|min:1|max:10',
        ];
    }
}
