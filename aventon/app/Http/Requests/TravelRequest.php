<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TravelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $date = date('Y-m-d');

        return [
            'departure_date'    => 'required|date|after:'. $date,
            'departure_time'    => 'required',
            'cost'              => 'required|int|Min:100|Max:9999',
            'departure_address' => 'required|string|min:3|max:255',
            'arrival_address'   => 'required|string|min:3|max:255',
            'arrival_date'      => 'required|date|after:'. $date,
            'arrival_time'      => 'required',
            'source_province'   => 'string|min:3|max:255',
            'source_locality'   => 'string|min:3|max:255',
            'destiny_province'  => 'string|min:3|max:255',
            'destiny_locality'  => 'string|min:3|max:255',
        ];
    }
}
