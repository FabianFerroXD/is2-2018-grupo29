<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Carbon\Carbon;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        /*
         * dt saca la fecha actual
         * before le resta 18 años a la fecha dt
         * en birthdate se compara que se cumpla
         */
        $dt = new Carbon();
        $before = $dt->subYears(18)->format('Y-m-d');
        
         return [
            'name'                => 'string|min:3|max:255',
            'lastname'            => 'string|min:3|max:255',
            'email'               => 'string|email|max:50|users->email',
            'password'            => 'min:6|max:255',
            'password_confirmed'  => 'min:6|max:255|same:password',
            'birthdate'           => 'date|before:' . $before,
         ];
    }
}