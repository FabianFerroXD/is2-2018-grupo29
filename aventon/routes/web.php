<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/**
 * !! ACÁ SE PUEDE MODIFICAR TODAS LAS RUTAS - IMPORTANTE !!
 * Verbos en el protocolo HTTP: get, post, put.
 */

Route::get('/', function () {
    $travels = App\Travel::orderBy('departure_date','ASC')->where('multiple_id', '=', NULL)->where('arrival_date', '>=', date('Y-m-d H:i:s'))->paginate(7);
    return view('welcome', compact('travels'));
});

// AUTH

// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')
        ->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')
        ->name('logout');

// Registration Routes...
Route::get('register', 'Auth\RegisterController@showRegistrationForm')
        ->name('register');
Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')
        ->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')
        ->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')
        ->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

/**
 * Rutas (cars, users, travels, questions, ratings) (Antes de los resources).
 */

// USERS
Route::pattern('users', '[0-9]+');
Route::get('/users/{user}/password', 'UserController@editPassword')
        ->name('users.edit_password');
Route::match(['put', 'patch'], '/users/{user}/password', 'UserController@updatePassword')
        ->name('users.update_password');
Route::get('/users/{user}/destroy', 'UserController@validateDestroy')
        ->name('users.validate_destroy');


// CARS
Route::pattern('cars', '[0-9]+');


// TRAVELS
Route::pattern('travels', '[0-9]+');
Route::get('/travels/{travel}/destroy', 'TravelController@validateDestroy')
        ->name('travels.validate_destroy');
Route::get('/travels/{travel}/selections', 'TravelController@showSelections')
        ->name('travels.show_selections');
Route::get('/travels/{travel}/passenger/{user}', 'TravelController@showPassenger')
        ->name('travels.show_passenger');
Route::get('/travels/search', 'TravelController@searchTravel')
    ->name('travels.search_travel');
Route::get('/travels/create_multiple', 'TravelController@createMultiple')
    ->name('travels.create_multiple');
Route::get('/travels/{travel}/edit_multiple', 'TravelController@editMultiple')
    ->name('travels.edit_multiple');
Route::match(['put', 'patch'], '/travels/{travel}/update_multiple', 'TravelController@updateMultiple')
    ->name('travels.update_multiple');
Route::post('/travels/store_multiple', 'TravelController@storeMultiple')
    ->name('travels.store_multiple');

// TRAVELS - POSTULANT
Route::get('/travels/{travel}/exit/{user}', 'TravelController@exitTravel')
        ->name('travels.exit_postulant');
Route::get('/travels/{travel}/exit_penal/{user}', 'TravelController@exitTravelWithPenal')
        ->name('travels.exit_postulant_penal');
Route::get('/travels/{travel}/delete/{user}', 'TravelController@deletePassenger')
        ->name('travels.delete_passenger');
Route::get('/travels/{travel}/postulants', 'TravelController@showPostulants')
        ->name('travels.show_postulants');
Route::get('/travels/{travel}/postulate', 'TravelController@postulate')
        ->name('travels.postulate');
Route::get('/travels/{travel}/reject/{user}', 'TravelController@rejectPostulant')
        ->name('travels.reject_postulant');
Route::get('/travels/{travel}/accept/{user}', 'TravelController@acceptPostulant')
        ->name('travels.accept_postulant');


// QUESTIONS
Route::pattern('questions', '[0-9]+');


// RATINGS
Route::pattern('ratings', '[0-9]+');
Route::get('/ratings/rate/{user}', 'RatingController@showRatings')
    ->name('ratings.show_ratings');


/**
 * Se genera CRUD's (index, store, create, destroy, update, show, edit).
 */

Route::resource('users','UserController');
Route::resource('cars','CarController');
Route::resource('travels','TravelController');
Route::resource('questions','QuestionController');
Route::resource('ratings','RatingController');

// HOME
Route::get('/home', 'HomeController@index')->name('home');

//CONTACTO
Route::get('/contact', 'ContactoController@contact')
        ->name('contact');

//AYUDA
Route::get('/ayuda', 'AyudaController@ayuda')
        ->name('ayuda');