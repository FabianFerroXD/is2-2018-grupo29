<?php

use Faker\Generator as Faker;
use App\Car;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Car::class, function (Faker $faker) {

    return [
        'patent'          => $faker->unique()->domainWord,
        'brand'           => $faker->domainWord,
        'model'           => $faker->lastName,
        'year'            => $faker->numberBetween($min = 2000, $max = 2019),
        'kind'            => $faker->randomElement($array = array ('Auto','Camioneta','Colectivo','Trafic','Camion')),
        'places'          => $faker->numberBetween(1,10),
    ];
});