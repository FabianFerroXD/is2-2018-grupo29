<?php

use Faker\Generator as Faker;
use App\Travel;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Travel::class, function (Faker $faker) {
    /*
     * Simulamos horarios de salida y llegada de viajes.
     */
    $departure_date_time = $faker->dateTimeBetween($startDate = '+ 1 day', $endDate = '+ 1 months');
    $arrival_date_time = $faker->dateTimeInInterval($startDate = $departure_date_time, $interval = '12 hours');

    return [
        'departure_date'      => $departure_date_time,
        'departure_time'      => $departure_date_time,
        'cost'                => $faker->numberBetween(100,9999),
        'departure_address'   => $faker->streetAddress,
        'arrival_address'     => $faker->streetAddress,
        'place_empty'         => $faker->numberBetween(1,10),
        'arrival_date'        => $arrival_date_time,
        'arrival_time'        => $arrival_date_time,
        'source_province'     => $faker->state,
        'source_locality'     => $faker->city,
        'destiny_province'    => $faker->state,
        'destiny_locality'    => $faker->city,
    ];
});