<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');

            $table->integer('question_id')->unsigned()->nullable();//este es el id de la pregunta que va a responder	+
            $table->foreign('question_id')->references('id')->on('questions');

            $table->integer('answer_id')->unsigned()->nullable();//este es el id de la pregunta que va a responder	+
            $table->foreign('answer_id')->references('id')->on('questions');
            /*
             * Si o si en 1 a muchos tiene que estar el id de la tabla
             * que lo tiene muchas veces.
             */
            $table->integer('travel_id')->unsigned();
            $table->foreign('travel_id')->references('id')->on('travels');
            $table->string('content', 200);
            $table->boolean('answer', false);


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
