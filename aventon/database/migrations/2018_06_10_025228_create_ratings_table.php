<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRatingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ratings', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('user_id')->unsigned();//este id va para que funcione la relacion de 1 a muchos
            $table->foreign('user_id')->references('id')->on('users');

            $table->integer('travel_id')->unsigned();//esto va si queremos saber de que viaje es la calificacion y se puede sacar fechas
            $table->foreign('travel_id')->references('id')->on('travels');

            $table->date('date_for_rating');//Fecha donde se va a permitir hacer la calificacion

            $table->integer('user_to_rating')->unsigned();//usuario al que se va a calificar
            $table->foreign('user_to_rating')->references('id')->on('users');

            $table->string('content', 200);
            $table->boolean('rate', false);
            $table->string('type', 200);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ratings');
    }
}
