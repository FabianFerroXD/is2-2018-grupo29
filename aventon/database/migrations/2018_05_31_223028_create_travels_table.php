<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTravelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('travels', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('car_id')->unsigned();
            $table->foreign('car_id')->references('id')->on('cars');
            $table->date('departure_date');
            $table->time('departure_time');
            $table->integer('cost');
            $table->string('departure_address');
            $table->string('arrival_address');
            $table->integer('place_empty');
            $table->date('arrival_date');
            $table->time('arrival_time');
            $table->string('source_province');
            $table->string('source_locality');
            $table->string('destiny_province');
            $table->string('destiny_locality');
            $table->integer('multiple_id')->unsigned()->nullable();
            $table->boolean('multiple', false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('travels');
    }
}
