<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

// APPS AVENTON
use App\User;
use App\Car;
use App\Travel;
use App\Question;
use App\Selection;
use App\Rating;

class UsersTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    // Crea 3 conductores.
    factory(User::class, 3)
      ->create(['account' => 100])
      ->each(function($driver)
      {
        // Crea 2 autos.
        $cars = factory(Car::class, 2)->create(['places' => 4]);

        foreach ($cars as $car)
        {
          $driver->cars()->save($car);

          // Crea viaje simple.
          $travel = factory(Travel::class)
                ->create(['car_id' => $car->id, 'user_id' => $driver->id, 'multiple_id' => NULL, 'multiple' => false]);
          $driver->travels()->save($travel);

          // Agrega 2 postulantes y 2 seleccionados al viaje.
          for ($i = 1; $i <= 2; $i++)
          {
            $postulant_user = factory(User::class)->create();
            $travel->users()->save($postulant_user);

            $selection_user = factory(User::class)->create();
            $selection = factory(Selection::class)
                  ->create(['user_id' => $selection_user->id, 'travel_id' => $travel->id]);
            $travel->selections()->save($selection);
            $selection_user->selection()->save($selection);

            // Crea calificaciones.
            factory(Rating::class)
                 ->create([ 'user_id' => $driver->id, 'travel_id' => $travel->id, 'date_for_rating' => $travel->arrival_date, 'user_to_rating' => $selection_user->id,'content'=>'','rate'=>false,'type'=>'']);

            factory(Rating::class)
                ->create([ 'user_id' => $selection_user->id, 'travel_id' => $travel->id, 'date_for_rating' => $travel->arrival_date, 'user_to_rating' => $driver->id,'content'=>'','rate'=>false,'type'=>'']);

            // Crea pregunta.
            $question = factory(Question::class)
                  ->create(['user_id' => $selection_user->id, 'travel_id' => $travel->id, 'question_id' => NULL, 'answer_id' => NULL, 'answer' => false]);
            $travel->questions()->save($question);
            $selection_user->questions()->save($question);
          }

          // Crea viaje princial del viaje multiple.
          $main_travel = factory(Travel::class)
                ->create(['car_id' => $car->id, 'user_id' => $driver->id, 'multiple_id' => NULL, 'multiple' => true]);
          $driver->travels()->save($main_travel);

          // Agrega dos viajes auxiliares al viaje multiple.
          for ($i = 1; $i <= 2; $i++)
          {
            factory(Travel::class)
                  ->create(['car_id' => $car->id, 'user_id' => $driver->id, 'multiple_id' => $main_travel->id, 'multiple' => true]);
          }

          // Agrega 2 postulantes y 2 seleccionados al viaje principal.
          for ($i = 1; $i <= 2; $i++)
          {
            $postulant_user = factory(User::class)->create();
            $main_travel->users()->save($postulant_user);

            $selection_user = factory(User::class)->create();
            $selection = factory(Selection::class)
                  ->create(['user_id' => $selection_user->id, 'travel_id' => $main_travel->id]);
            $main_travel->selections()->save($selection);
            $selection_user->selection()->save($selection);
          }

          // Crea calificaciones del viaje multiple.
          $last_date = $selection_user->arrival_date;

          foreach ($main_travel->selections() as $selection)
          {
            factory(Rating::class)
                 ->create([ 'user_id' => $driver->id, 'travel_id' => $travel->id, 'date_for_rating' => $last_date, 'user_to_rating' => $selection->id,'content'=>'','rate'=>false,'type'=>'']);

            factory(Rating::class)
                ->create([ 'user_id' => $selection->id, 'travel_id' => $travel->id, 'date_for_rating' => $last_date, 'user_to_rating' => $driver->id,'content'=>'','rate'=>false,'type'=>'']);
          }

          // Crea dos viaje viejos.
          for ($i = 1; $i <= 2; $i++)
          {
            $faker = Faker::create();

            $expired_arrival_date = $faker->dateTimeBetween($startDate = '-10 day', $endDate = 'now');
            $departure_date = $faker->dateTimeInInterval($startDate = $expired_arrival_date, $interval = '-12 hours');

            $old_travel = factory(Travel::class)
                  ->create(['car_id' => $car->id, 'user_id' => $driver->id, 'multiple_id' => NULL, 'multiple' => false, 'departure_date' => $departure_date, 'departure_time' => $departure_date, 'arrival_date' => $expired_arrival_date, 'arrival_time' => $expired_arrival_date]);
            $driver->travels()->save($old_travel);

            $selection_user = factory(User::class)->create();
            $selection = factory(Selection::class)
                  ->create(['user_id' => $selection_user->id, 'travel_id' => $old_travel->id]);
            $old_travel->selections()->save($selection);
            $selection_user->selection()->save($selection);

            factory(Rating::class)
                 ->create([ 'user_id' => $driver->id, 'travel_id' => $old_travel->id, 'date_for_rating' => $old_travel->arrival_date, 'user_to_rating' => $selection->id,'content'=>'','rate'=>false,'type'=>'']);

            factory(Rating::class)
                ->create([ 'user_id' => $selection->id, 'travel_id' => $old_travel->id, 'date_for_rating' => $old_travel->arrival_date, 'user_to_rating' => $driver->id,'content'=>'','rate'=>false,'type'=>'']);
          }
        }

        // Crea 1 auto.
        $car = factory(Car::class)->create(['places' => 2]);
        $driver->cars()->save($car);

        // Crea viaje individual sin seleccionados.
        $travel = factory(Travel::class)
              ->create(['car_id' => $car->id, 'user_id' => $driver->id, 'multiple_id' => NULL, 'multiple' => false]);
        $driver->travels()->save($travel);

        // Crea 1 auto sin viajes.
        $car = factory(Car::class)->create(['places' => 2]);
        $driver->cars()->save($car);
      });
  }
}
