<div class="form-group">
  {!! Form::label('departure_date', 'Fecha Salida:') !!}
  {!! Form::date('departure_date', null, ['class' => 'form-control', 'min' => date('Y-m-d'), 'required']) !!}
</div>

<div class="form-group">
  {!! Form::label('departure_time', 'Hora Salida:') !!}
  {!! Form::time('departure_time', null, ['class' => 'form-control', 'required']) !!}
</div>

<div class="form-group">
  {!! Form::label('arrival_date', 'Fecha Llegada:') !!}
  {!! Form::date('arrival_date', null, ['class' => 'form-control', 'min' => date('Y-m-d'), 'required']) !!}
</div>

<div class="form-group">
  {!! Form::label('arrival_time', 'Hora Llegada:') !!}
  {!! Form::time('arrival_time', null, ['class' => 'form-control', 'required']) !!}
</div>

<div class="form-group">
  {!! Form::label('departure_address', 'Dirección Salida:') !!}
  {!! Form::text('departure_address', null, ['class' => 'form-control', 'required']) !!}
</div>

<div class="form-group">
  {!! Form::label('arrival_address', 'Dirección Llegada:') !!}
  {!! Form::text('arrival_address', null, ['class' => 'form-control', 'required']) !!}
</div>

<div class="form-group">
  {!! Form::label('cost', 'Costo:') !!}
  {!! Form::number('cost', null, ['class' => 'form-control', 'readonly']) !!}
</div>

<div class="form-group">
  {!! Form::label('car_id', 'Patente (Vehiculo):') !!}<br>
  {!! Form::text('car_id', $travel->car->patent, ['class' => 'form-control', 'readonly']) !!}
</div>

<div class="form-group">
  {!! Form::label('source_province', 'Provincia de Origen:') !!}
  {!! Form::text('source_province', null, ['class' => 'form-control', 'required']) !!}
</div>

<div class="form-group">
  {!! Form::label('source_locality', 'Localidad de Origen:') !!}
  {!! Form::text('source_locality', null, ['class' => 'form-control', 'required']) !!}
</div>

<div class="form-group">
  {!! Form::label('destiny_province', 'Provincia de Destino:') !!}
  {!! Form::text('destiny_province', null, ['class' => 'form-control', 'required']) !!}
</div>

<div class="form-group">
  {!! Form::label('destiny_locality', 'Localidad de Destino:') !!}
  {!! Form::text('destiny_locality', null, ['class' => 'form-control', 'required']) !!}
</div>

<div class="form-group">
  {!! Form::submit('Cargar Viaje', ['class' => 'btn btn-primary']) !!}
</div>