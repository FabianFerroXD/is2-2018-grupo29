@extends('layouts.app')

@section('content')
  @include('layouts.alerts')
 
  <div class="row margenes-cajas">
    @if ( !$is_driver )
      <div class="card col-md-3 mx-auto">
        <div class="card-header">
          <h5>
            {{ __('Conductor del viaje: ').$driver->id }}
          </h5>
        </div>

        <div class="card-body">
          @include('layouts.show_driver')
        </div>
      </div>
    @endif

    <div class="card col-md-4 mx-auto">
      <div class="card-header">
        <h5>
          {{ __('Viaje con el auto de patente: ').$travel->car->patent }}
        </h5>
      </div>

      <div class="card-body">
        @if( $travel->multiple )
          @include('layouts.show_multiple_travel')
        @else
          @include('layouts.show_travel')
        @endif

        @if ( $is_driver )
          <div class="form-group">
            <a class="btn btn-primary btn-block" role="button" href="{{ route('travels.show_postulants', $travel) }}">
              <img src="{{ asset('iconic-svg/people.svg') }}" alt="{{ __('Postulantes') }}">  {{ __('Postulantes') }}
            </a>
          </div>
          <div class="form-group">
            <a class="btn btn-primary btn-block" role="button" href="{{ route('travels.show_selections', $travel) }}">
              <img src="{{ asset('iconic-svg/task.svg') }}" alt="{{ __('Seleccionados') }}">  {{ __('Seleccionados') }}
            </a>
          </div>
        @else
          <div class="form-group">
            <a class="btn btn-primary btn-block" role="button" href="{{ route('travels.postulate', $travel) }}">
              <img src="{{ asset('iconic-svg/plus.svg') }}" alt="{{ __('Postularme') }}">  {{ __('Postularme') }}
            </a>
          </div>
        @endif
      </div>
    </div>
    <div class="card col-md-4 mx-auto">
      @if ( $is_driver )
        <div class="card-header">
          <h5>
            {{ __('Preguntas :') }}
          </h5>
        </div>
        <div class="card-body">
            <p>
                @include('layouts.list_questions_drive')
            </p>
        </div>
      @else
        <div class="card-header">
          <h5>
            {{ __('Preguntar algo:') }}
          </h5>
        </div>
            <div class="card-body">
                <p>
                    @include('layouts.list_questions')
                </p>
            </div>
        @endif
    </div>
  </div>
@endsection