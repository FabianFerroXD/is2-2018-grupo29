@extends('layouts.app')

@section('content')
  @include('layouts.alerts')

  <div class="card col-md-4 mx-auto">
    <div class="card-header">
      <h5>
        {{ __('Usuario: ') }}

        <a class="btn btn-primary float-right" role="button" href="{{ route('travels.show', $travel) }}">
          {{ __('Volver') }}
        </a>
      </h5>
    </div>

    <div class="card-body">
      @include('layouts.show_passenger')
    </div>
  </div>
@endsection