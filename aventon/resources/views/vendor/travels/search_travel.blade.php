@extends('layouts.app')

@section('content')
    @include('layouts.alerts')

    <div class="box-index card col-md-11 mx-auto">
        <div class="card-header">
            <h5>
                <a class="btn btn-primary float-right" role="button" href="{{ route('home') }}">
                    {{ __('Volver') }}
                </a>
            </h5>
        </div>

        <div class="card-body">
            @include('layouts.list_travels_home')
        </div>
    </div>
@endsection