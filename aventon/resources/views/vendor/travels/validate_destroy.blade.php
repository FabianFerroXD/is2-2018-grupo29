@extends('layouts.app')

@section('content')
  @include('layouts.alerts')

  <div class="card col-md-5 mx-auto">
    <div class="card-header">
      <h5>
        {{ __('Eliminar viaje a' ) }}{{ $travel->source_locality }}

        <a class="btn btn-primary float-right" role="button" href="{{ route('users.show', Auth::user()) }}">
          {{ __('Volver') }}
        </a>
      </h5>
    </div>

    <div class="card-body">
      <h2>
        <p>{{ __('¿Estas seguro que quieres eliminar el viaje?') }}</p>
      </h2>
      <h5>
      <p>{{ __('Ten en cuenta que si eliminas el viaje tu calificacion como conductor será penalizada') }}</p>
      <h5>
      <div class="form-group">
        <form action="{{ route('travels.destroy', $travel->id) }}" method="post">
          {{ csrf_field() }}
          <input type="hidden" name="_method" value="DELETE">
          <button class="btn btn-primary float-right">
            <img src="{{ asset('iconic-svg/warning.svg') }}" alt="{{ __('Eliminar Viaje') }}">  {{ __('Eliminar Viaje') }}
          </button>
        </form>
      </div>
    </div>
  </div>
@endsection