@extends('layouts.app')

@section('content')
  @include('layouts.alerts')

  <div class="card col-md-5 mx-auto">
    <div class="card-header">
      <h5>
        {{ __('Viaje con el auto de patente: ').$travel->car->patent }}

        <a class="btn btn-primary float-right" role="button" href="{{ route('users.show', Auth::user()) }}">
          Volver
        </a>
      </h5>
    </div>

    <div class="card-body">
      {!! Form::model($travel, ['route' => ['travels.update_multiple', $travel], 'method' => 'PUT']) !!}

        @include('travels.fragment.form_edit_multiple')

      {!! Form::close() !!}
  </div>
@endsection