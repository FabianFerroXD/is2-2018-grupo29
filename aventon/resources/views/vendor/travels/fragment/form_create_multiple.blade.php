<div class="form-group">
  {!! Form::label('departure_date', 'Fecha del primer dia del viaje:') !!}
  {!! Form::date('departure_date', null, ['class' => 'form-control', 'min' => date('Y-m-d'), 'required']) !!}
</div>

<div class="form-group">
  {!! Form::label('departure_time', 'Hora de inicio de viajes:') !!}
  {!! Form::time('departure_time', null, ['class' => 'form-control', 'required']) !!}
</div>

<div class="form-group">
  {!! Form::label('arrival_date', 'Fecha del ultimo dia del viaje:') !!}
  {!! Form::date('arrival_date', null, ['class' => 'form-control', 'min' => date('Y-m-d'), 'required']) !!}
</div>

<div class="form-group">
  {!! Form::label('duration', 'Duración:') !!}
  {!! Form::text('duration', null, ['class' => 'form-control', 'required']) !!}
</div>

<div class="form-group">
  {!! Form::hidden('arrival_time', date('H:i:s'), null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
  <div class="form-group col-md-4 float-left">
    {!! Form::label('monday', 'Lunes') !!}
    {!! Form::checkbox('workday[]', 'monday') !!}
    <br>
    {!! Form::label('tuesday', 'Martes') !!}
    {!! Form::checkbox('workday[]', 'tuesday') !!}
  </div>

  <div class="form-group col-md-4 float-left">
    {!! Form::label('wednesday', 'Miércoles') !!}
    {!! Form::checkbox('workday[]', 'wednesday') !!}
    <br>
    {!! Form::label('thursday', 'Jueves') !!}
    {!! Form::checkbox('workday[]', 'thursday') !!}
  </div>

  <div class="form-group col-md-4 float-left">
    {!! Form::label('friday', 'Viernes') !!}
    {!! Form::checkbox('workday[]', 'friday') !!}
    <br>
    {!! Form::label('saturday', 'Sábado') !!}
    {!! Form::checkbox('workday[]', 'saturday') !!}
    <br>
    {!! Form::label('sunday', 'Domingo') !!}
    {!! Form::checkbox('workday[]', 'sunday') !!}
  </div>
</div>

<div class="form-group">
  {!! Form::label('cost', 'Costo:') !!}
  {!! Form::number('cost', null, ['class' => 'form-control', 'min' => '100', 'max' => '9999', 'required']) !!}
</div>

<div class="form-group">
  {!! Form::label('departure_address', 'Dirección Salida:') !!}
  {!! Form::text('departure_address', null, ['class' => 'form-control', 'required']) !!}
</div>

<div class="form-group">
  {!! Form::label('arrival_address', 'Dirección Llegada:') !!}
  {!! Form::text('arrival_address', null, ['class' => 'form-control', 'required']) !!}
</div>

<div class="form-group">
  {!! Form::label('car_id', 'Patente (Vehiculo):') !!}<br>
  {!! Form::select('car_id', Auth::user()->cars->pluck('patent', 'id'), null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
  {!! Form::label('source_province', 'Provincia de Origen:') !!}
  {!! Form::text('source_province', null, ['class' => 'form-control', 'required']) !!}
</div>

<div class="form-group">
  {!! Form::label('source_locality', 'Localidad de Origen:') !!}
  {!! Form::text('source_locality', null, ['class' => 'form-control', 'required']) !!}
</div>

<div class="form-group">
  {!! Form::label('destiny_province', 'Provincia de Destino:') !!}
  {!! Form::text('destiny_province', null, ['class' => 'form-control', 'required']) !!}
</div>

<div class="form-group">
  {!! Form::label('destiny_locality', 'Localidad de Destino:') !!}
  {!! Form::text('destiny_locality', null, ['class' => 'form-control', 'required']) !!}
</div>

<div class="form-group">
  {!! Form::hidden('multiple', 1, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
  {!! Form::hidden('multiple_id', NULL, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
  {!! Form::submit('Cargar Viaje Multiple', ['class' => 'btn btn-primary']) !!}
</div>