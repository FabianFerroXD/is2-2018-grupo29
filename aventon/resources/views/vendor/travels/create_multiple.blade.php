@extends('layouts.app')

@section('content')
  @include('layouts.alerts')

  <div class="card col-md-5 mx-auto">
    <div class="card-header">
      <h5>
        {{ __('Agregar viaje multiple') }}

        <a class="btn btn-primary float-right" role="button" href="{{ route('users.show', Auth::user()) }}">
          Volver
        </a>
      </h5>
    </div>

    <div class="card-body">
      <!-- Por defecto lo toma como POST -->
      {!! Form::open(['route' => 'travels.store_multiple']) !!}

        @include('travels.fragment.form_create_multiple')

      {!! Form::close() !!}
    </div>
  </div>
@endsection