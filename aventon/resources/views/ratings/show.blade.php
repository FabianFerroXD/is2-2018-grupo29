@extends('layouts.app')

@section('content')
    @include('layouts.alerts')

    <div class="row margenes-cajas">
        <div class="box-index card col-md-7 mx-auto">
            <div class="card-header">
                <h5>
                    {{ __('Calificaciones pendientes: ') }}
                    <a class="btn btn-primary float-right" role="button" href="{{ route('users.show', Auth::user()) }}">
                        {{ __('Volver') }}
                    </a>
                    <a class="btn btn-primary float-right" role="button" href="{{ route('ratings.show_ratings', Auth::user()) }}" style="margin-right: 10px;">
                      {{ __('Mis calificaciones') }}
                    </a>
                </h5>
            </div>
            <div class="card-body">
                @include('layouts.list_ratings')
            </div>
        </div>
    </div>
@endsection