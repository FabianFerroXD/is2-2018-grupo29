@extends('layouts.app')

@section('content')
    @include('layouts.alerts')
    <div class="row margenes-cajas">
        <div class="box-index card col-md-7 mx-auto">
            <div class="card-header">
                <h5>

                    <a class="btn btn-primary float-right" role="button" href="{{ route('ratings.show', Auth::user()) }}">
                        {{ __('Volver') }}
                    </a>
                    {{ __('Calificaciones recibidas: ') }}
                </h5>
            </div>
            <div class="card-body">
                @include('layouts.list_ratings_user')
            </div>
        </div>
    </div>

@endsection