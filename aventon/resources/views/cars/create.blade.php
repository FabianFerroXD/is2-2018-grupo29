@extends('layouts.app')

@section('content')
  @include('layouts.alerts')

  <div class="card col-md-5 mx-auto">
    <div class="card-header">
      <h5>
        {{ __('Agregar auto') }}

        <a class="btn btn-primary float-right" role="button" href="{{ route('users.show', Auth::user()) }}">
          {{ __('Volver') }}
        </a>
      </h5>
    </div>

    <div class="card-body">
      <!-- Por defecto lo toma como POST -->
      {!! Form::open(['route' => 'cars.store']) !!}

        {!! Form::label('patent', 'Patente:') !!}
        {!! Form::text('patent', null , ['class' => 'form-control', 'placeholder'=>'AA111AA'], 'required') !!}

        @include('cars.fragment.form')

      {!! Form::close() !!}
    </div>
  </div>
@endsection