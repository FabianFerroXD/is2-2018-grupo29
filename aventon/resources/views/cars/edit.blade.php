@extends('layouts.app')

@section('content')
  @include('layouts.alerts')

  <div class="card col-md-5 mx-auto">
    <div class="card-header">
      <h5>
        {{ __('Pantente: ').$car->patent }}

        <a class="btn btn-primary float-right" role="button" href="{{ route('users.show', Auth::user()) }}">
          {{ __('Volver') }}
        </a>
      </h5>
    </div>

    <div class="card-body">
      {!! Form::model($car, ['route' => ['cars.update', $car], 'method' => 'PUT']) !!}

        @include('cars.fragment.form')

      {!! Form::close() !!}
  </div>
@endsection