<div class="form-group">
  {!! Form::label('brand', 'Marca:') !!}
  {!! Form::text('brand', null , ['class' => 'form-control', 'placeholder'=>'Ford', 'required']) !!}
</div>

<div class="form-group">
  {!! Form::label('model', 'Modelo:') !!}
  {!! Form::text('model', null , ['class' => 'form-control', 'placeholder'=>'Fiesta', 'required']) !!}
</div>

<div class="form-group">
  {!! Form::label('year', 'Año:') !!}
  {!! Form::text('year', null , ['class' => 'form-control', 'placeholder'=>'2000', 'min' => '2000', 'max' => '2019', 'required']) !!}
</div>

<div class="form-group">
  {!! Form::label('kind', 'Tipo:') !!}<br>
  {!! Form::select('kind', ['Auto' => 'Auto', 'Camioneta' => 'Camioneta', 'Colectivo' => 'Colectivo', 'Trafic' => 'Trafic', 'Camion' => 'Camion'], null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
  {!! Form::label('places', 'Plazas:') !!}
  {!! Form::text('places', null , ['class' => 'form-control', 'placeholder'=>'4', 'min' => '1', 'max' => '10', 'required']) !!}
</div>

<div class="form-group">
  {!! Form::submit('Cargar Auto', ['class' => 'btn btn-primary']) !!}
</div>