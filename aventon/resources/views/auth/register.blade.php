@extends('layouts.app')

@section('content')
  @include('layouts.alerts')

  <div class="card col-md-5 mx-auto">
    <div class="card-header">
      <h5>
        {{ __('Datos Personales') }}
      </h5>
    </div>

    <div class="card-body">
      <form method="POST" action="{{ route('register') }}">
        @csrf

        <div class="form-group row">
          <label for="name" class="col-md-4 col-form-label text-md-right">
            {{ __('Nombre') }}
          </label>

          <div class="col-md-6">
            <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
          </div>
        </div>

        <div class="form-group row">
          <label for="lastname" class="col-md-4 col-form-label text-md-right">
            {{ __('Apellido') }}
          </label>

          <div class="col-md-6">
            <input id="lastname" type="text" class="form-control{{ $errors->has('lastname') ? ' is-invalid' : '' }}" name="lastname" value="{{ old('lastname') }}" required autofocus>
          </div>
        </div>

        <div class="form-group row">
          <label for="email" class="col-md-4 col-form-label text-md-right">
            {{ __('Correo Electronico') }}
          </label>

          <div class="col-md-6">
            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
          </div>
        </div>

        <div class="form-group row">
          <label for="password" class="col-md-4 col-form-label text-md-right">
            {{ __('Contraseña') }}
          </label>

          <div class="col-md-6">
            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
          </div>
        </div>

        <div class="form-group row">
          <label for="password-confirm" class="col-md-4 col-form-label text-md-right">
            {{ __('Confirmar Contraseña') }}
          </label>

          <div class="col-md-6">
            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
          </div>
        </div>

        <div class="form-group row">
          <label for="birthdate" class="col-md-4 col-form-label text-md-right">
            {{ __('Fecha de nacimiento') }}
          </label>

          <div class="col-md-6">
            <input id="birthdate" type="date" class="form-control{{ $errors->has('birthdate') ? 'is-invalid' : '' }}" name="birthdate" required>
          </div>
        </div>

        <div class="form-group row mb-0">
          <div class="col-md-6 offset-md-4">
            <button type="submit" class="btn btn-primary">
              {{ __('Confirmar') }}
            </button>
          </div>
        </div>
      </form>
    </div>
  </div>
@endsection