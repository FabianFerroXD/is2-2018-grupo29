@extends('layouts.app')

@section('content')


  <div class="card">
    <div class="card-header">
      <h1>
        Preguntas frecuentes:
      </h1>
    </div>
    <div class="card-header">
      <h4><p>- ¿Por qué puedo ver los viajes pero no puedo inscribirme?</p></h4>
      <p>- Para poder inscribirte debes crearte una cuenta en nuestro sitio e iniciar sesion.</p>
    </div>
      <div class="card-header">
      <h4><p>- ¿Cómo hago para crearme una cuenta en el sitio?</p></h4>
        <p>- Oprimiendo el boton "Registrarse" situado en la esquina superior derecha de la pagina y completando los datos.</p>
      </div>
      <div class="card-header">
   
      <h4><p>- Olvide mi contraseña ¿Cómo puedo recuperarla?</p></h4>
        <p>- Debes oprimir el boton "¿Olvidaste tu contraseña?" e ingresar tu mail asi nosotros te mandaremos un mail a tu casilla de correo con los pasos para recuperar tu contraseña.</p>
      
      </div>
      <div class="card-header">
      
      <h4><p>- ¿Como puedo cambiar los datos de mi cuenta? </p></h4>
        <p>- Para poder editar tus datos personales, debes entrar a tu perfil y seleccionar la opcion "Editar" .</p>
      
      </div>
      <div class="card-header">
     
      <h4><p>- ¿Por qué mi reputacion como conductor/pasajero ha disminudo si no he realizado ningún viaje?</p></h4>
        <p>- Es posible que hayas creado un viaje y luego haberlo dado de baja o te hayas dado de baja de algun viaje como pasajero lo que ocasiona que tu reputacion sea penalizada.</p>
      
      </div>
    </div>

  </div>
@endsection