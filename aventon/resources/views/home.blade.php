@extends('layouts.app')

@section('content')
  @include('layouts.alerts')

  <div class="row margenes-cajas">
    <div class="box-index card col-md-11 mx-auto">
      <div class="card-header">
      <h4>
        {{ __('Hola') }} <strong>{{ Auth::user()->name }}</strong> {{ __('bienvenido a Aventon') }}
      </h4>
    </div>
  </div>
  </div>
  <div class="row margenes-cajas">
    <div class="box-index card col-md-11 mx-auto">
      <div class="card-header">
        <h5>
          {{ __('Viajes que puedes postularte:') }}

          <div class="form-group margenes-cajas">
            {!! Form::open(['route' => 'travels.search_travel', 'method' => 'GET', 'class' => 'navbar-form navbar-left', 'role' => 'search'])  !!}

              <div class="input-group custom-search-form">
                <input type="text" class="form-control" name="search" maxlength="64" placeholder="Ingrese la localida de donde quiera viajar o localidad a la que quiere viajar" onfocus="this.value=''" required>
                <span class="input-group-btn">
                  <button class="btn btn-primary" role="button" type="submit">
                      Buscar Viaje
                  </button>
                </span>
              </div>

            {!! Form::close() !!}
          </div>
        </h5>
      </div>  
      <div class="card-body">
        @include('layouts.list_travels_home')
      </div>
    </div>
  </div>
  <div class="row margenes-cajas">
    <div class="box-index card col-md-11 mx-auto">
      <div class="card-header">
        <h5>
          Somos una empresa que surgió a partir de la idea de simplificar la planificacion de un viaje y abaratar sus costos. Porque nosotros entendemos la preparación y las dificultades que implica emprender un viaje, ya sea de corta o larga distancia, queremos ofrecerles opciones y variedades de destinos, según sus necesidades.  Nuestra misión es facilitar el acceso a diferentes destinos, dentro del país, con un servicio de transporte terrestre que prestan los usuarios confiable, seguro y accesible. Para esto, contamos con un sistema de calificación para que todos los usuarios cuenten con una referencia, sobre experiencias anteriores, al momento de emprender el viaje.
        </h5>
      </div>
    </div>
  </div>
@endsection