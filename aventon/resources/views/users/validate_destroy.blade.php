@extends('layouts.app')

@section('content')
  @include('layouts.alerts')

  <div class="card col-md-5 mx-auto">
    <div class="card-header">
      <h5>
        {{ Auth::user()->name }}

        <a class="btn btn-primary float-right" role="button" href="{{ route('users.show', Auth::user()) }}">
          Volver
        </a>
      </h5>
    </div>

    <div class="card-body">
      <h4>
        {{ __('¿Quieres eliminar tu cuenta?.') }}
      </h4>

      <div class="form-group">
        <form action="{{ route('users.destroy', Auth::user()) }}" method="post">
          {{ csrf_field() }}
          <input type="hidden" name="_method" value="DELETE">
          <button class="btn btn-primary float-right">
            <img src="{{ asset('iconic-svg/warning.svg') }}" alt="{{ __('Eliminar Cuenta') }}">  {{ __('Eliminar Cuenta') }}
          </button>
        </form>
      </div>
    </div>
  </div>
@endsection