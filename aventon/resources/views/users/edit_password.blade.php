@extends('layouts.app')

@section('content')
  @include('layouts.alerts')

  <div class="card col-md-5 mx-auto">
    <div class="card-header">
      <h5>
        {{ Auth::user()->name }}

        <a class="btn btn-primary float-right" role="button" href="{{ route('users.edit', Auth::user()) }}">
          Volver
        </a>
      </h5>
    </div>

    <div class="card-body">
      {!! Form::model($user, ['route' => ['users.update_password', Auth::user()], 'method' => 'PUT']) !!}

        <h3>
          {{ __('Nueva contraseña:') }}
        </h3>

        <div class="form-group row">
            <label for="password" class="col-md-4 col-form-label text-md-right">
                {{ __('Contraseña') }}
            </label>

            <div class="col-md-6">
                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
            </div>
        </div>

        <div class="form-group row">
            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">
                {{ __('Confirmar Contraseña') }}
            </label>

            <div class="col-md-6">
                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
            </div>
        </div>

        <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-4">
                <button type="submit" class="btn btn-primary">
                    <img src="{{ asset('iconic-svg/pencil.svg') }}" alt="{{ __('Modificar') }}">  {{ __('Modificar') }}
                </button>
            </div>
        </div>

      {!! Form::close() !!}
    </div>
  </div>
@endsection