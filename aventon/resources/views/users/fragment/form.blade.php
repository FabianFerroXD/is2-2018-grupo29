<div class="form-group">
  {!! Form::label('name', 'Nombre:') !!}
  {!! Form::text('name', null,['class' => 'form-control', 'placeholder'=>'Ingrese nombre', 'required']) !!}
</div>

<div class="form-group">
  {!! Form::label('lastname', 'Apellido:') !!}
  {!! Form::text('lastname', null, ['class' => 'form-control', 'placeholder'=>'Ingrese apellido', 'required']) !!}
</div>

<div class="form-group">
  {!! Form::label('birthdate', 'Fecha de nacimiento:') !!}
  {!! Form::date('birthdate', null, ['class' => 'form-control', 'required']) !!}
</div>

<div class="form-group">
  {!! Form::submit('Modificar', ['class' => 'btn btn-primary']) !!}
</div>