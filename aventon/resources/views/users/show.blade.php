@extends('layouts.app')

@section('content')
  @include('layouts.alerts')

  <div class="row margenes-cajas">
    <div class="box-index card col-md-5 mx-auto">
      <div class="card-header">
        <h5>
          {{ __('Mi usuario') }}

          <a class="btn btn-primary float-right" role="button" href="{{ route('users.edit', Auth::user()) }}">
            <img src="{{ asset('iconic-svg/pencil.svg') }}" alt="{{ __('Editar') }}">  {{ __('Editar') }}
          </a>
        </h5>
      </div>
      <div class="card-body">
        @include('layouts.show_user')
      </div>
    </div>

    <div class="box-index card col-md-4 mx-auto">
      <div class="card-header">
        <h5>
          {{ __('Mis autos') }}

          <a class="btn btn-primary float-right" role="button" href="{{ route('cars.create') }}">
            <img src="{{ asset('iconic-svg/plus.svg') }}" alt="{{ __('Nuevo auto') }}">  {{ __('Nuevo auto') }}
          </a>
        </h5>
      </div>
      <div class="card-body">
        @include('layouts.list_cars_user')
      </div>
    </div>
  </div>

  <div class="row margenes-cajas">
    <div class="box-index card col-md-10 mx-auto">
      <div class="card-header">
        <h5>
          {{ __('Mis viajes como conductor:  ') }}
          <button class="btn btn-primary dropdown-toggle float-right" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              {{ __('Crear viaje') }} <span class="caret"></span>
          </button>

          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">

            <a class="dropdown-item" href="{{ route('travels.create') }}">
              <img src="{{ asset('iconic-svg/plus.svg') }}" alt="{{ __('Simple') }}">  {{ __('Simple') }}
            </a>
            <a class="dropdown-item" href="{{ route('travels.create_multiple') }}">
              <img src="{{ asset('iconic-svg/plus.svg') }}" alt="{{ __('Multiple') }}">  {{ __('Multiple') }}
            </a>
          </div>
        </h5>
      </div>
      <div class="card-body">
        @include('layouts.list_travels_driver')
      </div>
    </div>
  </div>

  <div class="row margenes-cajas">
    <div class="box-index card col-md-10 mx-auto">
      <div class="card-header">
        <h5>
          {{ __('Mis viajes como pasajero: ') }}
        </h5>
      </div>
      <div class="card-body">
        @include('layouts.list_travels_selection')
      </div>
    </div>
  </div>

  <div class="row margenes-cajas">
    <div class="box-index card col-md-10 mx-auto">
      <div class="card-header">
        <h5>
          {{ __('Mis viajes como postulante: ') }}
        </h5>
      </div>
      <div class="card-body">
        @include('layouts.list_travels_postulant')
      </div>
    </div>
  </div>
@endsection