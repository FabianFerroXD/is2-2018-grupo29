@extends('layouts.app')

@section('content')
  @include('layouts.alerts')

  <div class="card col-md-5 mx-auto">
    <div class="card-header">
      <h5>
        {{ __('Correo electronico: ').Auth::user()->email }}

        <a class="btn btn-primary float-right" role="button" href="{{ route('users.show', Auth::user()) }}">
          {{ __('Volver') }}
        </a>
      </h5>
    </div>

    <div class="card-body">
      {!! Form::model(Auth::user(), ['route' => ['users.update', Auth::user()], 'method' => 'PUT']) !!}

        @include('users.fragment.form')

      {!! Form::close() !!}

      <div class="form-group">
        <a class="btn btn-primary float-left" role="button" href="{{ route('users.edit_password', Auth::user()) }}">
          <img src="{{ asset('iconic-svg/pencil.svg') }}" alt="{{ __('Cambiar Contraseña') }}">  {{ __('Cambiar Contraseña') }}
        </a>
      </div>

      <div class="form-group">
        <a class="btn btn-primary float-right" role="button" href="{{ route('users.validate_destroy', Auth::user()) }}">
          <img src="{{ asset('iconic-svg/trash.svg') }}" alt="{{ __('Eliminar Usuario') }}">  {{ __('Eliminar Usuario') }}
        </a>
      </div>
    </div>
  </div>
@endsection