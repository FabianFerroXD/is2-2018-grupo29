<div class="form-group">
    {!! Form::label('content', 'Pregunta:') !!}
    {!! Form::textarea('content', null , ['class' => 'form-control', 'placeholder' => 'Haz tu pregunta', 'required']) !!}
</div>

<div class="form-group">
    {!! Form::hidden('question_travel', $travel->id, ['class' => 'form-control', 'readonly']) !!}
</div>

<div class="form-group" >
    {!! Form::submit('Enviar pregunta', ['class' => 'btn btn-primary float-lg-right']) !!}
</div>