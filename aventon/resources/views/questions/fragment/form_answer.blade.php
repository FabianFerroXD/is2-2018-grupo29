<div class="form-group">
    {!! Form::label('content', 'Respuesta:') !!}
    {!! Form::textarea('content', null , ['class' => 'form-control', 'placeholder' => 'Haz tu respuesta', 'required']) !!}
</div>

<div class="form-group">
    {!! Form::hidden('question_travel', $question->travel_id, ['class' => 'form-control', 'readonly']) !!}
    {!! Form::hidden('question_id', $question->id , ['class' => 'form-control', 'readonly']) !!}
</div>

<div class="form-group" >
    {!! Form::submit('Enviar respuesta', ['class' => 'btn btn-primary float-lg-right']) !!}
</div>