<div class="table-responsive">
    <table class="table table-hover">
        <tbody> <!--aca tengo que hacer que este for recorra la tabla selection y por cada id de usuaior, buscar el usuario y mostrarlo-->
        <tr>
            <th><strong>Nombre:</strong></th>
            <th><strong>Apellido:</strong></th>
            <th><strong>Calificación como pasajero:</strong></th>
        </tr>
        <tr>
            <td>{!! $user->name !!}</td>
            <td>{!! $user->lastname !!}</td>
            <td>{!! $user->passenger_qualification !!}</td>
        </tr>
        </tbody>
    </table>
</div>