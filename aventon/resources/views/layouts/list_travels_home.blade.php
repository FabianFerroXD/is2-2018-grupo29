<div class="table-responsive">
    <table class="table table-hover">
        <thead>
        <tr>
            <th>Fecha Salida</th>
            <th>Hora Salida</th>
            <th>Costo</th>
            <th>Localidad de Salida</th>
            <th>Localidad de Llegada</th>
        </tr>
        </thead>
        <tbody>
        @foreach( $travels as $travel )
            <tr>
                <td>{{ $travel->departure_date }}</td>
                <td>{{ $travel->departure_time }}</td>
                <td>{{ $travel->cost }}</td>
                <td>{{ $travel->source_locality }}</td>
                <td>{{ $travel->destiny_locality }}</td>
                <td>
                    @if( $travel->multiple )
                      <a href="{{ route('travels.show', $travel) }}" class="btn btn-primary">
                          <img src="{{ asset('iconic-svg/eye.svg') }}" alt="{{ __('Ver Multiple') }}">  {{ __('Ver Multiple') }}
                      </a>
                    @else
                      <a href="{{ route('travels.show', $travel) }}" class="btn btn-primary">
                          <img src="{{ asset('iconic-svg/eye.svg') }}" alt="{{ __('Ver') }}">  {{ __('Ver') }}
                      </a>
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    {!! $travels->render() !!}
</div>