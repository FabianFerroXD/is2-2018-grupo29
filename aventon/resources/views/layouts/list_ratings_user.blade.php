<div class="table-responsive">
    <table class="table table-hover">
        <thead>
            <tr colspan="4">
                <th><h3>Positivas</h3></th>
            </tr>
            <tr>
                <th>Fecha del viaje</th>
                <th>Usuario</th>
                <th>Rol</th>
                <th>Comentario</th>
            </tr>
        </thead>
        <tbody>
            @foreach($ratings as $rating)
                @if($rating->type == 'Positiva')
                    <tr>
                        <td>{{ $rating->date_for_rating }}</td>
                        <td>{{ $rating->user->name }}</td>
                        @if( $rating->user_id == $rating->travel->user_id )
                          <td>Conductor</td>
                        @else
                          <td>Pasajero</td>
                        @endif
                        <td>{{ $rating->content }}</td>
                    </tr>
                @endif
            @endforeach
        </tbody>
    </table>
</div>

<div class="table-responsive">
    <table class="table table-hover">
        <thead>
            <tr colspan="4">
                <th><h3>Neutrales</h3></th>
            </tr>
            <tr>
                <th>Fecha del viaje</th>
                <th>Usuario</th>
                <th>Rol</th>
                <th>Comentario</th>
            </tr>
        </thead>
        <tbody>
            @foreach($ratings as $rating)
                @if($rating->type == 'Neutral')
                    <tr>
                        <td>{{ $rating->date_for_rating }}</td>
                        <td>{{ $rating->user->name }}</td>
                        @if( $rating->user_id == $rating->travel->user_id )
                          <td>Conductor</td>
                        @else
                          <td>Pasajero</td>
                        @endif
                        <td>{{ $rating->content }}</td>
                    </tr>
                @endif
            @endforeach
        </tbody>
    </table>
</div>

<div class="table-responsive">
    <table class="table table-hover">
        <thead>
            <tr colspan="4">
                <th><h3>Negativas</h3></th>
            </tr>
            <tr>
                <th>Fecha del viaje</th>
                <th>Usuario</th>
                <th>Rol</th>
                <th>Comentario</th>
            </tr>
        </thead>
        <tbody>
            @foreach($ratings as $rating)
                @if($rating->type == 'Negativa')
                    <tr>
                        <td>{{ $rating->date_for_rating }}</td>
                        <td>{{ $rating->user->name }}</td>
                        @if( $rating->user_id == $rating->travel->user_id )
                          <td>Conductor</td>
                        @else
                          <td>Pasajero</td>
                        @endif
                        <td>{{ $rating->content }}</td>
                    </tr>
                @endif
            @endforeach
        </tbody>
    </table>
</div>