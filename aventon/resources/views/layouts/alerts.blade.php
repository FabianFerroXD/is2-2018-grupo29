<div id="alerts" class="col-md-12">
  @if (session('status'))
    <div class="alert alert-success">
      {{ session('status') }}
    </div>
  @endif

  @include('cars.fragment.error')
  @include('cars.fragment.info')
</div>