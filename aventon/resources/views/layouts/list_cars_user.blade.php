<div class="table-responsive">
  <table class="table table-hover">
    <tbody>
      @foreach(Auth::user()->cars as $car)
      <tr>
        <td>
          <strong>
            {{ $car->patent }}
          </strong>
        </td>
        <td>
          <a href="{{ route('cars.show', $car) }}" class="btn btn-primary">
            <img src="{{ asset('iconic-svg/eye.svg') }}" alt="{{ __('Ver') }}">  {{ __('Ver') }}
          </a>
        </td>
        <td>
          <a href="{{ route('cars.edit', $car) }}" class="btn btn-primary">
            <img src="{{ asset('iconic-svg/pencil.svg') }}" alt="{{ __('Editar') }}"> {{ __('Editar') }}
          </a>
        </td>
        <td>
          <form action="{{ route('cars.destroy', $car) }}" method="post">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="DELETE">
            <button class="btn btn-primary">
              <img src="{{ asset('iconic-svg/trash.svg') }}" alt="{{ __('Borrar') }}"> {{ __('Borrar') }}
            </button>
          </form>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>