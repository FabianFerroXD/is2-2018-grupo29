<div class="table-responsive">
    <table class="table table-hover">
        <tbody> <!--aca tengo que hacer que este for recorra la tabla selection y por cada id de usuaior, buscar el usuario y mostrarlo-->
        <tr>
            <th><strong>Nombre:</strong></th>
        </tr>
        @foreach( $users as $user )
            <tr>
                <td>{!! $user->name !!}</td>
                <td>
                    <a class="btn btn-primary" role="button" href="{{ route('travels.show_passenger', ['travel'=> $travel->id, 'user' => $user->id]) }}">
                        <img src="{{ asset('iconic-svg/check.svg') }}" alt="{{ __('Ver') }}">  {{ __('Ver') }}
                    </a>
                </td>
                <td>
                    <a class="btn btn-primary" role="button" href="{{ route('travels.delete_passenger', ['travel'=> $travel, 'user' => $user->id]) }}">
                        <img src="{{ asset('iconic-svg/x.svg') }}" alt="{{ __('Eliminar') }}">  {{ __('Eliminar') }}
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>