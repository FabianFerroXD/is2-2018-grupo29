<div class="table-responsive">
  <table class="table table-hover">
    <tr>
      <td><strong>Nombre:</strong></td>
    </tr>
    <tr>
      <td>{!! $driver->name !!}</td>
    </tr>
    <tr>
      <td><strong>Apellido:</strong></td>
    </tr>
    <tr>
      <td>{!! $driver->lastname !!}</td>
    </tr>
    <tr>
      <td><strong>Calificación como conductor:</strong></td>
    </tr>
    <tr>
      <td>{!! $driver->driver_qualification !!}</td>
    </tr>
  </table>
</div>