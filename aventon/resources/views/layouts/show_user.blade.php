<div class="table-responsive">
  <table class="table table-hover">
    <tr>
      <td><strong>Nombre:</strong></td>
      <td>{!! Auth::user()->name !!}</td>
    </tr>
    <tr>
      <td><strong>Apellido:</strong></td>
      <td>{!! Auth::user()->lastname !!}</td>
    </tr>
    <tr>
      <td><strong>Correo electronico:</strong></td>
      <td>{!! Auth::user()->email !!}</td>
    </tr>
    <tr>
      <td><strong>Fecha de nacimiento:</strong></td>
      <td>{!! Auth::user()->birthdate !!}</td></tr>
    <tr>
      <td><strong>Calificación como conductor:</strong></td>
      <td>{!! Auth::user()->driver_qualification !!}</td>
    </tr>
    <tr>
      <td><strong>Calificación como pasajero:</strong></td>
      <td>{!! Auth::user()->passenger_qualification !!}</td>
    </tr>
    <tr>
      <td><strong>Cuenta:</strong></td>
      <td>{!! Auth::user()->account !!}</td>
    </tr>

  </table>
</div>