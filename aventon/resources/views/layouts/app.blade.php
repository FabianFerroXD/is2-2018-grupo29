<!-- ACÁ SE MODIFICA LA PAGINA PRINCIPAL -->
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
  <head>
    <link rel="shortcut icon" href="{{ asset('/img/favicon.ico') }}" >
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('', 'Aventon') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->


    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('css/style.css') }}" type="text/css" />
  </head>
  <body>
    <header>
      <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
        <div class="container">
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>

          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <a class="navbar-brand" href="{{ url('/') }}"><img src={{asset('/img/imagen.png')}} alt="Logo" height = "75" width="75">
              {{ __('Aventon') }}
            </a>
            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
              <!-- Authentication Links -->
              @guest
                <li>
                  <a class="nav-link" href="{{ route('login') }}">{{ __('Ingresar') }}</a>
                </li>
                <li>
                  <a class="nav-link" href="{{ route('register') }}">{{ __('Registrarse') }}</a>
                </li>
              @else
                <li class="dropdown">
                  <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      {{ Auth::user()->name }} <span class="caret"></span>
                  </button>

                  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">

                    <a class="dropdown-item" href="{{ route('home') }}">
                      <img src="{{ asset('iconic-svg/home.svg') }}" alt="{{ __('Home') }}">  {{ __('Home') }}
                    </a>
                    <a class="dropdown-item" href="{{ route('users.show', Auth::user()) }}">
                      <img src="{{ asset('iconic-svg/person.svg') }}" alt="{{ __('Mi perfil') }}">  {{ __('Mi perfil') }}
                    </a>
                    <a class="dropdown-item" href="{{ route('ratings.show', Auth::user()) }}">
                      <img src="{{ asset('iconic-svg/graph.svg') }}" alt="{{ __('Calificaciones') }}">  {{ __('Calificaciones') }}
                    </a>
                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                             document.getElementById('logout-form').submit();">
                      <img src="{{ asset('iconic-svg/account-logout.svg') }}" alt="{{ __('Salir') }}">  {{ __('Salir') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      @csrf
                    </form>

                  </div>
                </li>
              @endguest
            </ul>
          </div>
        </div>
      </nav>
    </header>

    <main>
      <div class="container-fluid margenes-cajas">
        @yield('content')
      </div>
    </main>

    <footer>
    <a class="nav-link btn-primary float-lg-left" href="{{ route('ayuda') }}">{{ __('Ayuda') }}</a>
      <a class="nav-link btn-primary float-lg-right" href="{{ route('contact') }}">{{ __('Contacto') }}</a>
        <p style="text-align: center">
          Derechos Reservados - Otelaf.
        </p>
    </footer>
  </body>
</html>
