<div class="table-responsive">
  <table class="table table-hover">
    <tr>
      <td><strong>Fecha Salida:</strong></td>
      <td>{{ $travel->departure_date }}</td>
    </tr>
    <tr>
      <td><strong>Fecha Llegada:</strong></td>
      <td>{{ $travel->arrival_date }}</td>
    </tr>
    @foreach($multiple_travel as $multiple)
      <tr>
        <td><strong>Fecha Salida:</strong></td>
        <td>{{ $multiple->departure_date }}</td>
      </tr>
      <tr>
        <td><strong>Fecha Llegada:</strong></td>
        <td>{{ $multiple->arrival_date }}</td>
      </tr>
    @endforeach
    <tr>
      <td><strong>Hora Salida:</strong></td>
      <td>{{ $travel->departure_time }}</td>
    </tr>
    <tr>
      <td><strong>Hora Llegada:</strong></td>
      <td>{{ $travel->arrival_time }}</td>
    </tr>
    <tr>
      <td><strong>Costo:</strong></td>
      <td>{{ $travel->cost }}</td>
    </tr>
    <tr>
      <td><strong>Dirección Salida:</strong></td>
      <td>{{ $travel->departure_address}}</td>
    </tr>
    <tr>
      <td><strong>Dirección Llegada:</strong></td>
      <td>{{ $travel->arrival_address }}</td>
    </tr>
    <tr>
      <td><strong>Plazas  Disponibles:</strong></td>
      <td>{!! $travel->car->places !!}</td>
    </tr>
    <tr>
      <td><strong>Origen Provincia:</strong></td>
      <td>{{ $travel->source_province }}</td>
    </tr>
    <tr>
      <td><strong>Origen Localidad:</strong></td>
      <td>{{ $travel->source_locality}}</td>
    </tr>
    <tr>
      <td><strong>Destino Provincia:</strong>
      </td><td>{{ $travel->destiny_province }}</td>
    </tr>
    <tr>
      <td><strong>Destino Localidad:</strong>
      </td><td>{{ $travel->destiny_locality}}</td>
    </tr>
  </table>
</div>