<div class="table-responsive">
  <table class="table table-hover">
    <thead>
      <tr>
        <th>Fecha Salida</th>
        <th>Hora Salida</th>
        <th>Costo</th>
        <th>Localidad de Salida</th>
        <th>Localidad de Llegada</th>
        <th>Plazas Vacias</th>
        <th>Fecha Llegada</th>
        <th>Hora Llegada</th>
        <th>Origen Provincia</th>
        <th>Destino Provincia</th>
        <th>Dirección Salida</th>
        <th>Dirección Llegada</th>
      </tr>
    </thead>
    <tbody>
      @foreach($travels as $travel)
        <tr>
          <td>{{ $travel->departure_date }}</td>
          <td>{{ $travel->departure_time }}</td>
          <td>{{ $travel->cost }}</td>
          <td>{{ $travel->source_locality }}</td>
          <td>{{ $travel->destiny_locality }}</td>
          <td>{{ $travel->car->places }}</td>
          <td>{{ $travel->arrival_date }}</td>
          <td>{{ $travel->arrival_time }}</td>
          <td>{{ $travel->source_province }}</td>
          <td>{{ $travel->destiny_province }}</td>
          <td>{{ $travel->departure_address }}</td>
          <td>{{ $travel->arrival_address }}</td>
        </tr>
      @endforeach
    </tbody>
  </table>

  {!! $travels->render() !!}
</div>