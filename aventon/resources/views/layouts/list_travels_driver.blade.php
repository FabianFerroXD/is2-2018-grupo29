<div class="table-responsive">
  <table class="table table-hover">
    <thead>
      <tr>
        <th>Fecha Salida</th>
        <th>Hora Salida</th>
        <th>Costo</th>
        <th>Localidad de Salida</th>
        <th>Localidad de Llegada</th>
        <th>Plazas Vacias</th>
      </tr>
    </thead>
    <tbody>
      @foreach( $drive_travels as $travel )
        <tr>
          <td>{{ $travel->departure_date }}</td>
          <td>{{ $travel->departure_time }}</td>
          <td>{{ $travel->cost }}</td>
          <td>{{ $travel->source_locality }}</td>
          <td>{{ $travel->destiny_locality }}</td>
          <td>{{ $travel->car->places }}</td>
          
          @if( $travel->multiple )
            <td>
              <a href="{{ route('travels.show', $travel) }}" class="btn btn-primary">
                <img src="{{ asset('iconic-svg/eye.svg') }}" alt="{{ __('Ver Multiple') }}">  {{ __('Ver Multiple') }}
              </a>
            </td>
            <td>
              <a href="{{ route('travels.edit_multiple', $travel) }}" class="btn btn-primary">
                <img src="{{ asset('iconic-svg/pencil.svg') }}" alt="{{ __('Editar') }}">  {{ __('Editar Multiple') }}
              </a>
            </td>
          @else
            <td>
              <a href="{{ route('travels.show', $travel) }}" class="btn btn-primary">
                  <img src="{{ asset('iconic-svg/eye.svg') }}" alt="{{ __('Ver') }}">  {{ __('Ver') }}
              </a>
            </td>
            <td>
              <a href="{{ route('travels.edit', $travel) }}" class="btn btn-primary">
                <img src="{{ asset('iconic-svg/pencil.svg') }}" alt="{{ __('Editar') }}">  {{ __('Editar') }}
              </a>
            </td>
          @endif

          <td>
            <a href="{{ route('travels.validate_destroy', $travel) }}" class="btn btn-primary">
              <img src="{{ asset('iconic-svg/trash.svg') }}" alt="{{ __('Borrar') }}">  {{ __('Borrar') }}
            </a>
          </td>
        </tr>
      @endforeach
    </tbody>
  </table>
</div>