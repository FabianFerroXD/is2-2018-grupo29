<div class="table-responsive">
    <table class="table table-hover">
        <tbody> <!--aca tengo que hacer que este for recorra la tabla selection y por cada id de usuaior, buscar el usuario y mostrarlo-->
        <tr>
            <th><strong>Nombre:</strong></th>
        </tr>
        @foreach( $travel->users as $postulant )
            @if ( $travel->user_id != $postulant->id )
            <tr>
                <td>{!! $postulant->name !!}</td>
                <td>
                    <a class="btn btn-primary" role="button" href="{{ route('travels.show_passenger', ['travel'=> $travel->id, 'user' => $postulant->id]) }}">
                        <img src="{{ asset('iconic-svg/check.svg') }}" alt="{{ __('Ver') }}">  {{ __('Ver') }}
                    </a>
                </td>
                <td>
                    <a class="btn btn-primary" role="button" href="{{ route('travels.accept_postulant', ['travel'=> $travel->id, 'user' => $postulant->id]) }}">
                        <img src="{{ asset('iconic-svg/check.svg') }}" alt="{{ __('Aceptar') }}">  {{ __('Aceptar') }}
                    </a>
                </td>
                <td>
                    <a class="btn btn-primary" role="button" href="{{ route('travels.reject_postulant', ['travel'=> $travel, 'user' => $postulant->id]) }}">
                        <img src="{{ asset('iconic-svg/x.svg') }}" alt="{{ __('Rechazar') }}">  {{ __('Rechazar') }}
                    </a>
                </td>
            </tr>
            @endif
        @endforeach
        </tbody>
    </table>
</div>