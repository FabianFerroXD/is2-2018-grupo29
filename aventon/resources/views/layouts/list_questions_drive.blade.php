<div class="table-responsive">
  <table class="table table-hover">
    <thead>
    <tr>
      <th>Usuario</th>
      <th>Pregunta</th>
    </tr>
    </thead>
    <tbody>
    @foreach($travel->questions as $question)
      <tr>
        <tr>
        @if($question->answer != true)
          <td>{{ $question->user->name }}</td>
          <td>{{ $question->content }}</td>
        @endif
        </tr>
        @if($question->answer_id != NULL)
          <td>  </td>
          <td colspan="2">{{ \App\Question::find($question->answer_id)->content }}</td>
        @endif
      <tr>
      @if ($question->question_id == null and  $question->answer == false  and  $question->answer_id == NULL)
        <tr>
          <td colspan="2">
            {!! Form::open(['route' => 'questions.store' ]) !!}
            @include('questions.fragment.form_answer')
            {!! Form::close() !!}
          </td>
        </tr>
      @endif
    @endforeach
    </tbody>
  </table>
</div>