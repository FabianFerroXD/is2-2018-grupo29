<div class="table-responsive">
    <table class="table table-hover">
        <thead>
        <tr>
            <th>Fecha disponible para calificar</th>
            <th>Usuario a calificar</th>
            <th>Rol</th>
        </tr>
        </thead>
        <tbody>
        @foreach($ratings as $rating)
            @if($rating->rate == false)
            <tr>
                <td>{{ $rating->date_for_rating }}</td>
                <td>{{ $rating->user_to_rating($rating)->name }}</td>
                @if( $rating->user_to_rating == $rating->travel->user_id )
                    <td> Conductor </td>
                @else
                    <td> Pasajero </td>
                @endif
            </tr>
            <tr>
              <td colspan="3">
                {!! Form::model($rating, ['route' => ['ratings.update', $rating], 'method' => 'PUT']) !!}
                  <div class="form-group">
                    {!! Form::label('qualification', 'Calificación:') !!}<br>
                    {!! Form::select('qualification', ['Positive' => 'Positivo', 'Neutral' => 'Neutral', 'Negative' => 'Negativo'], null, ['class' => 'form-control']) !!}
                  </div>
                  <div class="form-group">
                    {!! Form::label('content', 'Comentario:') !!}
                    {!! Form::textarea('content', null , ['class' => 'form-control ','rows'=>'3' ,'cols'=>'50', 'placeholder' => 'Ingrese comentario ', 'required', 'min'=>'5']) !!}
                  </div>
                  <div class="form-group">
                    {!! Form::hidden('rating_id', $rating->id, ['class' => 'form-control', 'readonly']) !!}
                  </div>
                  <div class="form-group">
                    {!! Form::submit('Calificar', ['class' => 'btn btn-primary']) !!}
                  </div>
                {!! Form::close() !!}
              </td>
            </tr>
            @endif
        @endforeach
        </tbody>
    </table>
    {!! $ratings->render() !!}
</div>