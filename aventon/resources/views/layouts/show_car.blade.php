<div class="table-responsive">
  <table class="table table-hover">
    <tr>
      <td><strong>Marca:</strong></td>
      <td>{{ $car->brand }}</td>
    </tr>
    <tr>
      <td><strong>Modelo:</strong></td>
      <td>{{ $car->model }}</td>
    </tr>
    <tr>
      <td><strong>Año:</strong></td>
      <td>{{ $car->year }}</td>
    </tr>
    <tr>
      <td><strong>Tipo:</strong></td>
      <td>{{ $car->kind }}</td>
    </tr>
    <tr>
      <td><strong>Plazas:</strong></td>
      <td>{!! $car->places !!}</td>
    </tr>
  </table>
</div>