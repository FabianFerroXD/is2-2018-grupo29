@extends('layouts.app')

@section('content')
  @include('layouts.alerts')

  <div class="card">
    <div class="card-header">
      <h5>
        {{ __('Viajes') }}
      </h5>
    </div>

    <div class="card-body">
      @include('layouts.list_travels')
    </div>
  </div>
@endsection