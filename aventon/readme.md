### Para que funcione:

##Base de datos:

* Crear base de datos vacia.
* cp .env.example .env (nombre, usuario y clave base de datos)
* Editar .env

##No olvidar:

* composer update

##Artisan (Carpeta Proyecto):

* php artisan key:generate
* php artisan migrate:refresh --seed
